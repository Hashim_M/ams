<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ ?>	
<?PHP 
require'config.php';
			
			if(isset($_POST['login-btn'])){
				global $item_id;
	$user = $_SESSION['user'];
	$req_typ =mysqli_real_escape_string($conn,$_POST['req_type']);
	$item_typ =mysqli_real_escape_string($conn,$_POST['item-req']);
	$descrip = mysqli_real_escape_string($conn,$_POST['desc']);
	
	
	
	$query =  "INSERT INTO request (requester,req_type,item,description,date_submitted)
	                      VALUES('$user','$req_typ','$item_typ','$descrip',now())";
    $run_query = $conn->query($query);						  
	 
			
		if($run_query === TRUE){
		echo "<script>alert('REQUEST IS SUCCESSFULLY SUBMITTED')</script>";
		header("location:view-request.php"); 
	 }else{
	 
	 echo "<script>alert('REQUEST HAS FAILED')</script>";
	 
	 }}
	     
	?><!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
   <script type="text/javascript">
  

function formValidator(){
	// Make quick references to our fields
	var firstname = document.getElementById('firstname');
	var addr = document.getElementById('addr');
	var zip = document.getElementById('zip');
	var state = document.getElementById('state');
	var username = document.getElementById('username');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(firstname, "Please enter only letters for your name")){
		if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
			if(isNumeric(zip, "Please enter a valid zip code")){
				if(madeSelection(state, "Please Choose a contract type")){
					if(lengthRestriction(username, 15, 30)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>

</head>

<body>

    <div id="wrapper">
       
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">REQUEST ASSET </h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
             
						
                  
					            <form role="form" name="form" method="post" onsubmit="return formValidator()" action="request_asset.php">
                            

                                        <label for="sel1">Request Type:</label>
                                        <select class="form-control" id="state" name="req_type">
										<option selected value = "..select..">..\select\..</option>
                                        <?php 
										       $sel_group = "SELECT * FROM request_type";
											   $run_query = $conn->query($sel_group);
											    
												while($row_group = mysqli_fetch_array($run_query)){
												
										       
													 
													 echo"<option  value='";
													 echo $row_group ['request_type']; 
													 echo " '>";
													echo $row_group ['request_type']; 
													echo "</option>";
										} ?>
                                       
                                        </select>
                        
                         
                                    <label for="email">Item Requested:</label>
                                    <input type="text" placeholder="Table" id="firstname" class="form-control"  name="item-req" size="50" required/>
                                </br>
                             
                          
                                   <label for="comment">Request Description:</label>
                                   <textarea class="form-control" rows="5" id="username" name="desc" required></textarea>
                                </br>
                             
                             
                                   <button type="submit" name="login-btn" OnClick="confirm('Do you want to make the request?')" class="btn btn-success">Request</button>
                                   <button type="reset" class="btn btn-primary">Reset</button>
                                </form>  

	                      

    </div>
    <!-- /#wrapper -->


</body>
<?php }?>
</html>
