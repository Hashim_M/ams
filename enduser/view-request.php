		<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ ?>	
		<?php  
																
										require'config.php';
							/* $res = mysqli_query($conn,"SELECT * FROM  users") or die(mysql_error());
							 
						 while($row_res = mysqli_fetch_assoc($res)){ */
						 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>

</head>

<body>
 <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"s>EURS</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
               <a class="navbar-brand"  href="#">
														   <?php
									if (isset($_SESSION["user"]))
									  echo "Welcome " . $_SESSION["user"] . "!";
									else
									  echo "Welcome Admin!";
									?>
                    </a>
                
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                       <li><a href="profile.php?profile='profile'"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="home.php"><i class="fa fa-dashboard fa-fw"></i> DASHBOARD</a>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i>Asset Groups<span class="fa arrow"></span></a>
                             
							 <ul class="nav nav-second-level">
                                
							   <li>
                                    <a href="home.php?group='furnitures'"><i class="fa fa-chevron-circle-right"></i> Furnitures </a>
									<ul class="nav nav-second-level">
									<?php
									
						 require"config.php";
						 $res = "SELECT DISTINCT category FROM item_categories WHERE item_group_id = '1'" ;
							
							$result = $conn->query($res);

						
								// output data of each row
								while($row = mysqli_fetch_array($result)) {
					                      $category = $row['category'];
										 
			              echo "<li>";
						  echo'<a href="home.php?category=$category;">';
						  echo "<i class='fa fa-plus-circle'></i>";
						  echo $category;
						  echo "</a></li>"; 
                                }?>	</ul>
										
                              </li>
							  
							  <li>
                                    <a href="home.php?group='farmmachines'"><i class="fa fa-chevron-circle-right"></i>Farm Machines</a>
									<ul class="nav nav-second-level">
									
									
						 	<?php
									
						 require"config.php";
						 $res = "SELECT DISTINCT category FROM item_categories WHERE item_group_id = '3' " ;
							
							$result = mysqli_query($conn, $res);

							if (mysqli_num_rows($result) > 0) {
								// output data of each row
								while($row = mysqli_fetch_assoc($result)) {
					                      $category = $row['category'];
			               echo "<li>";
						  echo'<a href="home.php?category=$category;">';
						  echo "<i class='fa fa-plus-circle'></i>";
						  echo $category;
						  echo "</a></li>"; 
                                }}?>
									
										</ul>
                              </li>
							    <li>
                                    <a href="home.php?group='buildings'"><i class="fa fa-chevron-circle-right"></i>Buildings</a>
									<ul class="nav nav-second-level">
								
									   <li><a href="locations.php?locations ='location'"><i class='fa fa-plus-circle'></i>Location</a></li>
									</ul>
                              </li>
							  <li>
                                    <a href="home.php?group='car'"><i class="fa fa-chevron-circle-right"></i>Vehicles</a>
									
									<ul class="nav nav-second-level">
									
									<?php
									
						  require"config.php";
						 $res = "SELECT DISTINCT category FROM item_categories WHERE item_group_id = '2' " ;
							
							$result = mysqli_query($conn, $res);

							if (mysqli_num_rows($result) > 0) {
								// output data of each row
								while($row = mysqli_fetch_assoc($result)) {
					                      $category = $row['category'];
			              echo "<li>";
						  echo'<a href="home.php?category=$category;">';
						  echo "<i class='fa fa-plus-circle'></i>";
						  echo $category;
						  echo "</a></li>"; 
                                }}?>
									
									</ul>
                              </li>
							        
							  <li>
                                   <a href="home.php?group='land'"><i class="fa fa-chevron-circle-right"></i>Land</a>
								   <ul class="nav nav-second-level">
									
								<?php
									
						  require"config.php";
						 $res = "SELECT DISTINCT category FROM item_categories WHERE item_group_id = '4' " ;
							
							$result = mysqli_query($conn, $res);

							if (mysqli_num_rows($result) > 0) {
								// output data of each row
								while($row = mysqli_fetch_assoc($result)) {
					                      $category = $row['category'];
			              echo "<li>";
						  echo'<a href="home.php?category=$category;">';
						  echo "<i class='fa fa-plus-circle'></i>";
						  echo $category;
						  echo "</a></li>"; 
                                }}?>
									
									</ul>
                              </li>
							  
                               
                            </ul>
							
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="#"><i class="fa fa-puzzle-piece"></i>University Management<span class="fa arrow"></span></a>
							 <ul class="nav nav-second-level">
							
									
							<?php
									
						  require 'config.php';
						 $res = "SELECT  affliate_name FROM affliation";
						 
						$result = $conn->query($res);

						if ($result === TRUE) {
						
						while($row = mysqli_fetch_assoc($result)) {
					               $affln_name = $row['affliate_name'];
								   
			             echo "<li><a href='home.php?level='$affln_name'><i class='fa fa-chevron-circle-right'></i>".$affln_name."</a></li>"; 
                                }}
								?>
									
									
									</ul>
							
							
							
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Requisition Management<span class="fa arrow"></span></a>
							
							 <ul class="nav nav-second-level">
                               <li>
                                    <a href="view-request.php?request='request'"><i class="fa fa-chevron-circle-right"></i> Request<span class="fa arrow"></span></a>
                              </li>
							 </ul>
                            
                        </li>
						<li>
                            <a href="#"><i class="fa fa-th fa-fw"></i>Contract Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
							<li>
                                    <a href="contract.php?contract='contract'"><i class="fa fa-chevron-circle-right"></i>Contractors <span class="fa arrow"></span></a>
                              </li>
							 </ul>
                        </li>
						<li>
                            <a href="#"><i class="fa fa-suitcase fa-fw"></i> Assignment Management<span class="fa arrow"></span></a>
                           <ul class="nav nav-second-level">
                               <li>
                                    <a href="view-assignment.php?assign='assign'"><i class="fa fa-chevron-circle-right"></i> Assign<span class="fa arrow"></span></a>
                              </li>
							   <li>
                                    <a href="asset-transfer.php"><i class="fa fa-chevron-circle-right"></i> Asset Transfer <span class="fa arrow"></span></a>
                              </li>
							 </ul>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> About Us<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <li>
                                    <a href="#">020 087 23146</a>
                              </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                    <h1 class="page-header"> Requisition</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List of Requests&nbsp; &nbsp;
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
							
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
								
                                 <thead>
				        <tr>
						     <th><input type="checkbox" name ="checkbox[]" id="select-all" /></th>
                            
							 <th>Item Requested</th>
							 <th>Request Description</th>
							 <th>Request Type</th>
						     <th>Edit Request</th>
							 <th>Status</th>
						     <th>Delete Action</th>
						</tr>
				  </thead>
				  
				 
			      <tbody>
                                        
															<?php  
								
						 if(isset($_GET['request'])){
						$select_query = mysqli_query($conn,"SELECT * FROM request ") or die($conn->error);
						 while($row_res = mysqli_fetch_array($select_query)){?>
							 
							 <tr>
						     <th><input type="checkbox" name="checkbox[]" value="<?php echo $row_res['request_id'];?>" id="select-all2" /></th>
							 <td><?php echo $row_res['item'];?></td>
							 <td><?php echo $row_res['description'];?></td>
							 <td><?php echo $row_res['req_type'];?></td>
							 
							 <td><a rel="facebox" href="edit_request.php?edit=<?php echo $row_res['request_id'] ; ?>"class="btn btn-success"></i>Edit </a></td>
						     <td><a rel="facebox" href="request_status.php?edit=<?php echo $row_res['request_id'] ; ?>" class="btn btn-primary"> Status</a></td>
                             <td><a rel="facebox" href="delete.php?del=<?php echo $row_res['request_id'] ; ?>"class="btn btn-danger">Cancel</a></td>
						</tr>
							 
							
						   
						   
						
						<?php }}?>
                                        
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="well">
                                <p> 
								<a rel="facebox" href="request_asset.php"><i class="fa fa-plus fa-fw"></i>Request Asset</a>
								
								</p>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>
<?php } ?>
</html>
