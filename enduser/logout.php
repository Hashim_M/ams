<?php

//start session
session_start();

//set session data to an  empty array
$_SESSION = array();

//expire their cookie files

if( isset($_COOKIE["user"]) && isset($_COOKIE["pass"]) ){
	
	
	setcookie("user", '', strtotime('-5 days'), '/');
	setcookie("pass", '', strtotime('-5 days'), '/');
	
	} 

//destroy the session variables
session_destroy();

// double check to see if their sessions exists
   
   if(isset($_SESSION["user"])){
	   
	   
	  header("location: group.php?msg=Error:_Logout_Failed");
	   }else
	   {
		header("location:../index.php");   
		   
       }




?>