<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../pages/index.php');	
	
	}else{ ?>	
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>

</head>

<body>

    
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">EUARS</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <a class="navbar-brand"  href="#">
														   <?php
									if (isset($_SESSION["user"]))
									  echo "Welcome " . $_SESSION["user"] . "!";
									else
									  echo "Welcome Admin!";
									?>
                    </a>
					
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                      <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Enduser Management<span class="fa arrow"></span></a>
                             
							 <ul class="nav nav-second-level">
                                 <!--<li>
                                    <a href="login.html"><i class="fa fa-sign-in fa-fw"></i> Login</a>
                              </li> -->
							 
							   <li>
                                    <a href="tables.php"><i class="fa  fa-angle-right fa-fw"></i> Assets</a>
                              </li>
							  <li>
                                    <a href="locations.php"><i class="fa  fa-angle-right fa-fw"></i> Locations</a>
                              </li>
							  <li>
                                     <a href="faculty.php"><i class="fa  fa-angle-right fa-fw"></i> Faculty</a>
                              </li>
							  <li>
                                    <a href="Department.php"><i class="fa  fa-angle-right fa-fw"></i> Department</a>
                              </li>
                               
                            </ul>
							
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>Requisition  Management<span class="fa arrow"></span></a> 
							
							 <ul class="nav nav-second-level">
                               <li>
                                    <a href="view-request.php"><i class="fa  fa-angle-right fa-fw"></i> Requisition</a>
                              </li>
							 </ul>
                            
                        </li>
						<li>
                            <a href="#"><i class="fa fa-th fa-fw"></i> Contract Management<span class="fa arrow"></span></a>
                        <!--<ul class="nav nav-second-level">
							<li>
                                    <a href="contract.php"><i class="fa  fa-angle-right fa-fw"></i>Contractors</a>
                              </li>
							 </ul>-->
                        </li>
						<li>
                            <a href="#"><i class="fa fa-suitcase fa-fw"></i> Assignment Management<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                               <li>
                                    <a href="view-assignment.php"><i class="fa  fa-angle-right fa-fw"></i> Assign<span class="fa arrow"></span></a>
                              </li>
							 </ul>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> About Us<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <li>
                                    <a href="login.html">0705 840 752</a>
                              </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                    <h1 class="page-header"> Assets</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List of Assets &nbsp; &nbsp;
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
							
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
								
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="select-all2" /></th>
                                            <th>ITEM</th>
                                            <th>S/NO</th>
											
											<th>Status</th>
                                            <th>DESCRIPTION</th>
                                            
                                        </tr>
                                    </thead>
									
									 <tfoot>
				       <tr>
						     <th><input type="checkbox" id="select-all2" /></th>
                          
							<th>Item Name</th>
							<th>S/NO</th>
							
							<th>Status</th>
							 <th>Description</th>
							 
                             
					
						</tr>
				  </tfoot>
                                    <tbody>
                                        
																<?php  
																
										$conn=mysql_connect("localhost","root","");
										  
										  if(!$conn){
											  
										die(mysql.error());	  
										  }
											  
										$select_db =mysql_select_db("project",$conn);


										 if(!$select_db){
											  
										die(mysql_error());	  
										  }
						 $res = mysql_query("SELECT * FROM  items") or die(mysql_error());
						 
						 while($row_res = mysql_fetch_array( $res)){?>
							 
							 <tr>
						     
							<td><input type="checkbox" name="checkbox[]" value="<?php echo $row_res['req_id'];?>" id="select-all2" /></td>
							 <td><?php echo $row_res['item'];?></td>
							 <td><?php echo $row_res['serial_number'];?></td>
							
							   <td><?php echo $row_res['status'];?></td>
							 <td><?php echo substr($row_res['item_description'],0,20);?>&nbsp;<b><a rel="facebox" href="item-details.php?id=<?php echo $row_res['item_id'] ; ?>">Read more</a></b></td>
							<!-- <td><a rel="facebox" href="edit_asset.php?edit= echo $row_res['item_id'"><i class="fa fa-edit fa-fw"></i>Edit </a></td>
                             <td><a rel="facebox" href="delete_assets.php?del= echo $row_res['item_id']"><i class="fa fa-trash-o fa-fw"></i>Delete </button></a></td>
						-->
						</tr>
							 
							<?php }?>
                                        
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="well">
                               <!-- <p><a rel="facebox" href="add-asset.php"><i class="fa fa-plus fa-fw"></i>Add Asset</a>
							&nbsp; &nbsp; <a  href="sql_table_to_pdf/item-report.php"><i class="fa fa-floppy-o fa-fw"></i>View Reports</a>
		                    &nbsp; &nbsp; <a  rel="facebox" href="import_items.php"><i class="fa  fa-upload fa-fw"></i>Import Data</a></p> -->
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>
<?php } ?>
</html>
