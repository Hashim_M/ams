<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require'config.php';
   
	if(isset($_GET['edit'])){
		
		$edit_id = $_GET['edit'];
		
		
		 $edit_query=" SELECT * FROM request_type WHERE request_type_id='$edit_id' LIMIT 1 ";
	 
	 $run_edit = mysqli_query($conn, $edit_query) or die(mysqli_error($conn));
	 
	 while($edit_row=mysqli_fetch_array($run_edit)){ 
		
	
		$request_type= $edit_row['request_type'];
		$request_desc=$edit_row['request_type_description'];
		
	 
	
			
			
	?>	
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
   <script type="text/javascript">
  	function formValidator(){
	// Make quick references to our fields
	var cat = document.getElementById('asset-cat');
	var code = document.getElementById('asset-code');
	var description = document.getElementById('cat-desc');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(cat, "Please enter only letters for your faculty name")){
				if(isNumeric(code, "Please enter only numerics")){
					
						
							return true;
						
					
				}
				}
	
	
	return false;
	
}
	
function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}


  
  </script>

</head>

<body>

    <div id="wrapper">
      
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">EDIT REQUEST TYPE</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
            
                          
			                     <form role="form" name="form" method="post" onsubmit="return formValidator()" action="edit-request-type.php?edit_form=<?php echo $edit_row['request_type_id'];?>">
									<label for ="email">Request Type:</label><br />
									<input name="request-type" value="<?php echo $request_type; ?>" type="text" id="asset-cat" class="form-control" required/><br />
									<label for ="email">Request Type Description:</label><br />
									<textarea col="3" rows="3" name="description" id="username" class="form-control" ><?php echo $request_desc; ?></textarea>

									</br>
									<button type="submit" name="login-btn" onClick="confirm('Are you sure, you want to update the request type')" class="btn btn-success">Edit Request Type</button>
									<button type="reset" class="btn btn-primary">Reset</button>
									</form>
									<?php }}?>

									<?php 
									if(isset($_POST['login-btn'])){
												
										$edit_id = mysqli_real_escape_string($conn,$_GET['edit_form']);			
										$request_type = mysqli_real_escape_string($conn,$_POST['request-type']);
										$description =mysqli_real_escape_string($conn,$_POST['description']);

											$update_query ="UPDATE `request_type` SET `request_type`='$request_type',`request_type_description`='$description',
											`date_modified`=now() WHERE request_type_id ='$edit_id'";
											
											if(mysqli_query($conn,$update_query) === TRUE) 

										
											{
											echo "<script>alert('The request type is Successfully updated.')</script>";
											echo "<script>window.open('request-type.php','_self')</script>";
											}
											else
											{
												die(mysqli_error($conn));
											}
										  }
										
										?>




    </div>
    <!-- /#wrapper -->

    
</body>
<?php }?>
</html>
