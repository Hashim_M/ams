<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	
  
 require 'config.php';
	 
	if(isset($_GET['edit'])){
		
		
		$edit_id = $_GET['edit'];
		
		
		 $edit_query="SELECT * FROM request where request_id='$edit_id ' LIMIT 1";
	 
	 $run_edit = $conn->query($edit_query);
	 
	 while($edit_row=mysqli_fetch_array($run_edit)){
		
		 $request_id = $edit_row['request_id'];
		$item= $edit_row['item'];
		$requster =$edit_row['requester'];
		$description = $edit_row['description'];
		$request_type = $edit_row['req_type'];
		
	
	  
?>
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
  
   <script type="text/javascript">
  

function formValidator(){
	// Make quick references to our fields
	var firstname = document.getElementById('firstname');
	var addr = document.getElementById('addr');
	var zip = document.getElementById('zip');
	var state = document.getElementById('state');
	var username = document.getElementById('username');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(firstname, "Please enter only letters for your name")){
		if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
			if(isNumeric(zip, "Please enter a valid zip code")){
				if(madeSelection(state, "Please Choose a contract type")){
					if(lengthRestriction(username, 15, 30)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>

</head>

<body>

    <div id="wrapper">
       
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">APPROVE REQUEST </h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
             
                 
					 <form role="form" name="form" method="post" action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>?edit_form=<?php echo $edit_row['request_id'];?>" onsubmit='return formValidator()'>
                             <div class="form-group">
                               <label for="email">Requestor:</label>
                               <input type="text"   value="<?php echo $requster;?>" class="form-control"  name="requestor"  >
                            </div>
                         <div class="form-group">
                                        <label for="sel1">Request Type:</label>
										<select name="req-type" class="form-control">
                                        <?php 
										       $sel_group = "SELECT * FROM  request_type"; 
											   $res = $conn->query($sel_group);
											    
												while($row_group = mysqli_fetch_array($res)){
												
										            
													 
													 echo"<option  value='";
													 echo $row_group ['request_type']; 
													 echo " '>";
													echo $row_group ['request_type']; 
													echo "</option>";
										} ?>
                                       
                                       
                                        </select>
                         </div>
                          <div class="form-group">
                                    <label for="email">Item Requested:</label>
                                    <input  type="text"  value="<?php echo $item; ?>" class="form-control"  name="item-req" size="50" >
                            </div>
                             
                            <div class="form-group">
                                   <label for="comment">Request Description:</label>
                                   <textarea class="form-control"  rows="2"  name="desc"  ><?php echo $description ; ?></textarea>
                            </div>
                             
							<div class="form-group">
							       <label for="comment">First Approver:</label>
								   <input type="text" id="firstname" value=" " class="form-control"  name="status" size="50" required/>
							</div>
							<div class="form-group">
							       <label for="comment">Second Approver:</label>
								   <input type="text" disabled  value=" " class="form-control"  name="status1" size="50" >
							</div>
							<div class="form-group">
							       <label for="comment">Third Approver:</label>
								   <input type="text" disabled  value=" " class="form-control" name="status2" size="50" >
							</div>
                             
                                   <button type="submit" name="login-btn" class="btn btn-success">Approve Request</button>
                                   <button type="reset" class="btn btn-primary">Reset</button>
                   
				   </form> 
				   <?php }} ?>
				   
				   


								<?php
									
									 if(isset($_POST['login-btn']))
									  {
									  global $item_id;
										$edit_id = mysqli_real_escape_string($conn,$_GET['edit_form']);
										$requestor1 = mysqli_real_escape_string($conn,$_POST['requestor']);
										$r_type=mysqli_real_escape_string($conn,$_POST['req-type']);
										$item_req1=mysqli_real_escape_string($conn,$_POST['item-req']);
										$rdesc=mysqli_real_escape_string($conn,$_POST['desc']);
									    $status=mysqli_real_escape_string($conn,$_POST['status']);
										$approver = $_SESSION['user'];
									
										
										$update_query ="UPDATE `request` SET `requester`='$requestor1',`req_type`='$r_type',`item`='$item_req1',`description`='$rdesc',
										`approved_by`='$approver',`date_approved`=now(), `status` = '$status',status1='', status2='' WHERE request_id ='$edit_id'";
										
										if(mysqli_query($conn,$update_query) === true) 

									
										{
										echo "<script>alert('Request Successfully Approved.')</script>";
										echo "<script>window.open('view_request.php','_self')</script>";
										}
										else
										{
											die (mysqli_error($conn));
										}
									  }
									
									?>
									
	                       
    </div>
  

</body>
<?php }?>
</html>
