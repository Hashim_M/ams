<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require 'config.php';
	 
	if(isset($_GET['edit'])){
		global $affln_id;
		global $faculty;
		$edit_id = $_GET['edit'];
		
		
		 $edit_query="SELECT * FROM affliate_department WHERE affln_depart_id='$edit_id ' LIMIT 1";
	 
	 $run_edit =$conn->query($edit_query); 
	 
	 while($edit_row= mysqli_fetch_array($run_edit)){
		
		$depart_id = $edit_row['affln_depart_id'];
		$depart_name= $edit_row['affln_depart_name'];
		$depart_code =$edit_row['affln_depart_code'];
		$affln_id =$edit_row['affln_id']; 
		
	
	  

        $sel_query = "SELECT affln_name  FROM affliate_faculty WHERE affln_id='$affln_id' ";
		$result = $conn->query( $sel_query);
	  while ($row_data = mysqli_fetch_array($result)){
		
		$faculty= $row_data['affln_name'];
 		
		}
  ?>
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
  
   <script type="text/javascript">
  	function formValidator(){
	// Make quick references to our fields
	var cat = document.getElementById('asset-cat');
	var code = document.getElementById('asset-code');
	var description = document.getElementById('cat-desc');
		var s-code = document.getElementById('s-code');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(cat, "Please enter only letters for your faculty name")){
				if(isNumeric(code, "Please enter only numerics")){
					if(isAlphanumeric(s-code, "Numbers and Letters Only")){
						
							return true;
						
					
				}
				}
				}
	
	
	return false;
	
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
	}
function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}


  
  </script>

</head>

<body>

    <div id="wrapper">
       
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">EDIT DEPARTMENT</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
              
                          
			                     <form role="form" name="form" onsubmit="return formValidator()" method="post" action="edit_department.php?edit_form=<?php echo $edit_row['affln_depart_id'];?>">
									<label for="email">Faculty:</label><br />
									<select name="faculty" class="form-control" >
									<option value="SCIENCE"><?php echo $faculty;?></option>
									<?php 
										       $sel_group = "SELECT * FROM  affliate_faculty";
											   $result = $conn->query( $sel_group);
											    
												while($row_group =mysqli_fetch_array($result)){
												
										            
													 
													 echo"<option  value='";
													 echo $row_group ['affln_name']; 
													 echo " '>";
													echo $row_group ['affln_name']; 
													echo "</option>";
										} ?>
									
									</select>
									</br>
									<label for="email">Department Name:</label><br />
									<input name="name1"  type="text" id="asset-cat" value= "<?php echo $depart_name;?>" class="form-control" required/><br />
									<label for="email">Department Code:</label><br />
									<input name="code1" type="text" value="<?php echo $depart_code;?>" id="s-code" class="form-control" required/><br />

									</br>
									<button type="submit" name="login-btn" onClick="confirm('Are you sure, you want to save the update')" class="btn btn-success">Update Department</button>
									<button type="reset" class="btn btn-primary">Reset</button>
									</form>
									<?php }}?>
									
									<?php
										
										 if(isset($_POST['login-btn']))
										  {
										  global $faculty_id ;
											$edit_id = mysqli_real_escape_string($conn,$_GET['edit_form']);
											$depart_name =  mysqli_real_escape_string($conn,$_POST['name1']);
											$depart_code = mysqli_real_escape_string($conn,$_POST['code1']);
											$faculty =  mysqli_real_escape_string($conn,$_POST['faculty']);
											
											$sel_query = mysqli_query($conn,"SELECT affln_id FROM affliate_faculty WHERE affln_name LIKE  '% $faculty %' ") or die(  mysql_error());
										    while ($row_data = mysqli_fetch_array($sel_query )){
											
											$faculty_id = $row_data['affln_id'];
											
											}
											
											$update_query ="UPDATE `affliate_department` SET `affln_depart_name`='$depart_name',`affln_id`='$faculty_id',`affln_depart_code`='$depart_code',
											`date_modified`=now() WHERE affln_depart_id ='$edit_id'";
											
									        $res=$conn->query($update_query) or die(mysqli_error($conn));
										
											echo "<script>alert('Affliate Department is Successfully updated.')</script>";
											echo "<script>window.open('Department.php','_self')</script>";
											
										  }
										
										?>

                            
                        
    </div>
    <!-- /#wrapper -->

    
</body>
<?php }?>
</html>
