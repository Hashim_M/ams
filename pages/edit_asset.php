<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 

  require'config.php';
	
	 
	if(isset($_GET['edit'])){
		
		$edit_id = $_GET['edit'];
		
		
		 $edit_query="SELECT * FROM items WHERE item_id = '$edit_id ' LIMIT 1";
	 
	 $run_edit = $conn->query($edit_query);
	 
	 while($edit_row=mysqli_fetch_assoc($run_edit)){ 
		
		 
		
	
	  
  ?>	
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
  
  <script type="text/javascript">
 

function formValidator(){
	// Make quick references to our fields
	var name = document.getElementById('asset-name');
	var s-code = document.getElementById('s-code');
	var code = document.getElementById('t-code');
	var state = document.getElementById('state');
	var description = document.getElementById('description');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(name, "Please enter only letters for your name")){
		if(isAlphanumeric(s-code, "Numbers and Letters Only")){
			if(isNumeric(code, "Please enter a valid tracking code")){
				if(madeSelection(state, "Please Choose approriate selection")){
					if(lengthRestriction(description, 6, 8)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>
</script><script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
	$("#parent_cat").change(function() {
		$(this).after('<div id="loader"><img src="images/loading.gif" alt="loading category" /></div>');
		$.get('loadsubcat.php?parent_cat=' + $(this).val(), function(data) {
			$("#sub_cat").html(data);
			$('#loader').slideUp(200, function() {
				$(this).remove();
			});
		});	
    });

});
</script>

</head>

<body>

    <div id="wrapper">
      
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">EDIT ITEMS</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
              
                          
			                        
							 <form action="edit_asset.php?edit_form=<?php echo $edit_row['item_id'];?>" method= "post" onsubmit="return formValidator()" >
							 	 <label for="group">ASSET GROUP:</label>
										 <select name="parent_cat" class="form-control"  id="parent_cat">
										  <option value="select">select</option>    
										 <?php 
										       $sel_group ="SELECT * FROM item_group";
											   $result = $conn->query($sel_group);
											    
												while($row_group = mysqli_fetch_array($result)){
												
										       
													 
													 echo"<option  value='";
													 echo $row_group ['item_group_name']; 
													 echo " '>";
													echo $row_group ['item_group_name']; 
													echo "</option>";
										} ?>
										 </select>
							 <label for="category">ASSET CATEGORY:</label>
										 <select class="form-control" name="sub_cat"  id="sub_cat">
										 
										 </select>
										 
								 
								 
								 
							  <label for="location">LOCATION:</label>
								 <select class="form-control" name="location">
								  <?php 
										       $sel_group = "SELECT * FROM location";
											    $run_query = $conn->query( $sel_group);
												while($row_group = mysqli_fetch_array( $run_query)){
												
										           
													 
													 echo"<option  value='";
													 echo $row_group ['loc_name']; 
													 echo " '>";
													echo $row_group ['loc_name']; 
													echo "</option>";
										} ?>
								 </select> 
								 
								  
								    
							   <label for ="asset_name">ASSET NAME</label>
							   <input type="text" name="asset" size="40" id="asset-name"class="form-control" value="<?php echo $edit_row['item'];?>"required/>
								
	
								
								 <label for ="asset_name">SERIAL NUMBER</label>
							   <input type="text" name="sr-code" size="40" id="s-code" value="<?php echo $edit_row['serial_number'];?>"class="form-control" required/>
							
							   
							  
							    <label for ="asset value">QUANTITY</label>
							   <input type="text" name="qty" size="40" id="t-code" class="form-control" value="<?php echo $edit_row['qty'];?>" required/>
								
							   
							   <label for ="warranty-duration">WARRANTY DURATION</label>
							   <input type="text" name="asset-warranty" id="s-code" size="40" class="form-control" value="<?php echo $edit_row['warranty_duration'];?>" required/>
								
							   
							   <label for ="category_description">ASSET DESCRIPTION</label>
							   <textarea  colspan="5" rowspan="2" id="description" name="asset-desc" class="form-control"><?php echo $edit_row['item_description'];?></textarea>
								
								
							   <label for ="status">ASSET STATUS</label>
							   <input type="radio" name="status" size="40"  value="GOOD" required/> GOOD &nbsp;&nbsp;
							   <input type="radio" name="status" size="40"  value="FAULTY" required/> FAULTY
							    <input type="radio" name="status" size="40"  value="BOOKED" required/> BOOKED
								
					</br>
						<button type="submit" name="login-btn" onClick="confirm('Are you sure, you want to save the update')" class="btn btn-success">Update Category</button>
						<button type="reset" class="btn btn-primary">Reset</button>
						</form>
						<?php }}?>

						<?php
							
							 if(isset($_POST['login-btn']))
							  {
							global $item_id;
							global $item_group_id;
							global $cat_id;
							global $loc_id;
							    $edit_id = trim(mysqli_real_escape_string($conn,$_GET['edit_form']));
								$group= trim(mysqli_real_escape_string($conn,$_POST['parent_cat']));
								$category= trim(mysqli_real_escape_string($conn,$_POST['sub_cat']));
								$locate = trim(mysqli_real_escape_string($conn,$_POST['location']));
								$asset = mysqli_real_escape_string($conn,$_POST['asset']);	
								$sr_code = mysqli_real_escape_string($conn,$_POST['sr-code']);
							
								$asset_warranty = mysqli_real_escape_string($conn,$_POST['asset-warranty']);
								$asset_desc = mysqli_real_escape_string($conn,$_POST['asset-desc']);
								$status= mysqli_real_escape_string($conn,$_POST['status']);
								$qty= mysqli_real_escape_string($conn,$_POST['qty']);
								$modifier = $_SESSION["user"];
								
								
								$sel_cat_id = "SELECT cat_id,item_group_id FROM item_categories WHERE category ='$category' LIMIT 1";
									$run_query = $conn->query($sel_cat_id) or die(mysqli_error($conn));
                                      if($run_query === TRUE){									
									while($row_data = mysqli_fetch_array($run_query)){
									 
									 $item_group_id = $row_data ['item_group_id'];
									 $cat_id = $row_data ['cat_id']; 
									 }}
									$sel_cat_id2 = "SELECT loc_id FROM location WHERE loc_name ='$locate' LIMIT 1"; 
									$run_query1 = $conn->query($sel_cat_id2) or die(mysqli_error($conn));
                                      
									 while($row_data = mysqli_fetch_array($run_query1)){
									 $loc_id = $row_data ['loc_id'];			 
									 
									 }
								$update_query ="UPDATE `items` SET `item`='$asset ',group_id='$item_group_id',cat_id=' $cat_id',loc_id='$loc_id', 
									 serial_number='$sr_code',qty='$qty',warranty_duration=' $asset_warranty ',
									 item_description = '$asset_desc', date_modified= now(), status ='$status' WHERE item_id ='$edit_id'";
								
								$result=$conn->query($update_query) or die(mysqli_error($conn));
															
							 $selct_item_id ="SELECT item_id FROM items ORDER BY item_id DESC LIMIT 1";
							 $result=$conn->query($selct_item_id) or die(mysqli_error($conn));
		         
            				 while($row = mysqli_fetch_array( $result)){
				             $item_id = $row['item_id'];
							
				                       }
								$update_inventory = "UPDATE inventory SET item_id ='$item_id', qty='$qty',status='$status',
								modified_by='$modifier', date_modified = now() WHERE item_id='$item_id'";

							  $update_result = $conn->query($update_inventory ) or die(mysqli_error($conn));
							  
							    echo "<script>alert('Asset is successfully updated.')</script>";
								echo "<script>window.open('tables.php','_self')</script>";
							  
							  }
							$conn->close();
							?>

                            
                     

    </div>
    <!-- /#wrapper -->

    

</body>
<?php }?>
</html>
