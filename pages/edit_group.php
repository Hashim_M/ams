<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ ?>
<?php 
			
require'config.php';
	
	 
	if(isset($_GET['edit'])){
		
		$edit_id = $_GET['edit'];
		
		
		 $edit_query="SELECT * FROM item_group WHERE item_group_id='$edit_id ' LIMIT 1";
	 
	 $run_edit = mysqli_query($conn, $edit_query);
	 
	 while($edit_row=mysqli_fetch_array($run_edit)){ 
		
		 
		
	
	  
  ?>	
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
  <script type="text/javascript">
  	function formValidator(){
	// Make quick references to our fields
	var cat = document.getElementById('asset-cat');
	var group = document.getElementById('group');
	var description = document.getElementById('cat-desc');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(cat, "Please enter only letters for your group name")){
				if(madeSelection(group, "Please Choose a group")){
					if(lengthRestriction(description, 30, 150)){
						
							return true;
						
					}
				}
				}
	
	
	return false;
	
}
	
function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}
function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}


  
  </script>

</head>

<body>

    <div id="wrapper">
       
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">EDIT GROUP</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
                
            
                          
			                        
							 <form role="form" name="form" onsubmit="return formValidator()" method="post" action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>?edit_form=<?php echo $edit_row['item_group_id'];?>">
       
								   <label for ="group_name">GROUP NAME</label>
								   <input type="text" name="asset" id="asset-cat" class="form-control" value="<?php echo $edit_row['item_group_name']; ?>" size="40" required/>
									</br>
									 </br>
								   
								   <label for ="group_description">GROUP DESCRIPTION</label>
								   <textarea colspan="5" rowspan="2" name="description" id="cat-desc" class="form-control" ><?php echo  $edit_row['item_group_desc'];?></textarea>
									</br>
									 </br>
									 
									<button type="submit" name="login-btn" onClick="confirm('Are you sure, you want to save the update')" class="btn btn-success">Update Group</button>
								    <a href="group.php"><button class="btn btn-primary" type="submit">Back</button></a>
									</form>
									<?php }}?>
									<?php
										
										 if(isset($_POST['login-btn']))
										  {
										  $edit_id = mysqli_real_escape_string($conn,$_GET['edit_form']);
										 $group = mysqli_real_escape_string($conn,$_POST['asset']);
										$group_desc = mysqli_real_escape_string($conn,$_POST['description']);
											
											
											$update_query ="UPDATE `item_group` SET `item_group_name`='$group ',`item_group_desc`='$group_desc',`date_of_modification`=now()
											 WHERE item_group_id ='$edit_id'";
											
											if(mysqli_query($conn,$update_query)) 

										
											{
											echo "<script>alert('Asset Group is Successfully updated.')</script>";
											echo "<script>window.open('group.php','_self')</script>";
											}
											else
											{
												echo $conn->error;
											}
										  }
										
										?>

    </div>
    <!-- /#wrapper -->

    
</body>
<?php }?>
</html>
