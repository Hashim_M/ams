<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ ?>
<?php 
			
require 'config.php';
  
			
			if(isset($_POST['login-btn'])){
			global $faculty_id;	
$request_type = mysqli_real_escape_string($conn,$_POST['request-type']);
$description =mysqli_real_escape_string($conn,$_POST['description']);
	$query =  "INSERT INTO request_type(request_type,request_type_description,date_modified) VALUES ('$request_type','$description', now())";
	$res = $conn->query($query);
	         
			 
		if($res === TRUE)

	
		{
		echo "<script>alert('The REQUEST TYPE is Successfully Saved.')</script>";
		echo "<script>window.open('request-type.php','_self')</script>";
		}
		else
		{
			echo $conn->error;
		}
	
	 
	
			
			}
			?>
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
   <script type="text/javascript">
  	function formValidator(){
	// Make quick references to our fields
	var cat = document.getElementById('asset-cat');
	var group = document.getElementById('group');
	var description = document.getElementById('cat-desc');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(cat, "Please enter only letters for your group name")){
				if(madeSelection(group, "Please Choose a group")){
					if(lengthRestriction(description, 30, 150)){
						
							return true;
						
					}
				}
				}
	
	
	return false;
	
}
	
function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}
function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}


  
  </script>

</head>

<body>

    <div id="wrapper">
      
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">ADD REQUEST TYPE</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
              
                          
			                     <form action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>" method="post" onsubmit='return formValidator()'>
								<label for ="email">Request Type:</label><br />
									<input name="request-type"  type="text" id="asset-cat" class="form-control" required/><br />
									<label for ="email">Request Type Description:</label><br />
									<textarea col="3" rows="3" name="description" id="username" class="form-control" ></textarea>

									</br>
									<button type="submit" name="login-btn" onClick="confirm('Are you sure, you want to add the request type')" class="btn btn-success">Add Request Type</button>
									<a href="request-type.php"><button type="reset" class="btn btn-primary">Back</button></a>
									</form>
							
								

                            
                     
    </div>
    <!-- /#wrapper -->

    

</body>
<?php }?>
</html>
