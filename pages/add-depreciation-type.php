	<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require'config.php';
     if(isset($_POST['submit'])){
	   
	  
	   $dep_formula =  mysqli_real_escape_string($conn,$_POST['formula']);
	   $dep_type=  mysqli_real_escape_string($conn,$_POST['type']);
	   $dep_rate =  mysqli_real_escape_string($conn,$_POST['rate']);
	   $dep_description =  mysqli_real_escape_string($conn,$_POST['description']);
		 
		
		 
		 
		 $insert_query = "INSERT INTO depreciation(depreciation_type,depreciation_rate,depreciation_formula,depreciation_description,date_modified)
		 VALUE('$dep_type','$dep_rate','$dep_formula','$dep_description', now())";
		 
		 if(mysqli_query($conn, $insert_query))
		 
		{
		echo "<script>alert('Depreciation  is Successfully added.')</script>";
		echo "<script>window.open('depreciation.php','_self')</script>";
		}
		else
		{
			echo $conn->error;
		}
		 
		 }
		
		
		

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  <script type="text/javascript">
  

function formValidator(){
	// Make quick references to our fields
	var firstname = document.getElementById('firstname');
	var addr = document.getElementById('addr');
	var zip = document.getElementById('zip');
	var state = document.getElementById('state');
	var username = document.getElementById('username');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(firstname, "Please enter only letters for your name")){
		if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
			if(isNumeric(zip, "Please enter a valid zip code")){
				if(madeSelection(state, "Please Choose a contract type")){
					if(lengthRestriction(username, 15, 30)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>

  
  
 

</head>

<body>

    <div id="wrapper">
        <!-- Page Content -->
        <!--<div id="page-wrapper">-->
                 <!--<div class="container-fluid">-->
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">ADD DEPRECIATION TYPE</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Depreciation
                        </div>
                        <!-- /.panel-heading 
                        <div class="panel-body">-->
                          
			                      <form action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>" method= "post" enctype="multipart/form-data" onsubmit="return formValidator()">
									 
									   <label for ="depreciation">DEPRECIATION TYPE</label>
									   <input type="text" name="type" size="40" id="addr" class="form-control" required/>
										</br>
										<label for ="rate">DEPRECIATION RATE</label>
									   <input type="text" name="rate" size="40" id= "addr" class="form-control" placeholder="10%" required/>
										</br>
									  <label for ="telephone">DEPRECIATION FORMULA</label>
									   <input type="text" name="formula" size="40" id="addr"class="form-control" placeholder="asset-value * rate" required/>
										</br>
										
									   
									   <label for ="category_description">DEPRECIATION DESCRIPTION</label>
									   <textarea  colspan="3" rowspan="3" name="description" id="username" class="form-control"></textarea>
										</br>
										
									   
									  <button type="submit" name="submit" class="btn btn-success">Add Depreciation</button></h1>
									  <button type="reset" name="submit"  class="btn btn-danger">Reset</button></h1>
									   
									 </form>



                            
                        <!--</div>
                         /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>
<?php }?>
</html>
