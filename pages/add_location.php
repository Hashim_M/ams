<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require 'config.php';
  
			
			if(isset($_POST['login-btn'])){
		
			global $building_id;	
			
	$loc_name = mysqli_real_escape_string($conn,$_POST['name']);
	$loc_code =mysqli_real_escape_string($conn,$_POST['code']);
	$loc_acronym =mysqli_real_escape_string($conn,$_POST['acronym']);
	$loc_descrip =mysqli_real_escape_string($conn,$_POST['description']);
    $building = mysqli_real_escape_string($conn,$_POST['sub_cat']);
	 $status = mysqli_real_escape_string($conn,$_POST['status']);
	
	
	
	$sel_query1 = "SELECT building_id FROM building WHERE building_name ='$building' LIMIT 1 ";
	$result = $conn->query($sel_query1);
	while ($row_data = mysqli_fetch_array($result)){
		
		$building_id = $row_data['building_id'];
 		
		}
	
	
	$query ="INSERT INTO location(loc_code,loc_acronym,loc_name,description,building_id,status,date_modified) 
	VALUES('$loc_code','$loc_acronym','$loc_name','$loc_descrip','$building_id','$status', now())"; 
	       
		   if($conn->query($query))
		{
		echo "<script>alert('The Location is Successfully added.')</script>";
		echo "<script>window.open('locations.php','_self')</script>";
		}
		else
		{
			echo "error in saving the records";
		}
	
	 
	
			
			}
			?>
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
   <script type="text/javascript">
  

function formValidator(){
	// Make quick references to our fields
	var firstname = document.getElementById('firstname');
	var addr = document.getElementById('addr');
	var zip = document.getElementById('zip');
	var state = document.getElementById('state');
	var username = document.getElementById('username');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(firstname, "Please enter only letters for your name")){
		if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
			if(isNumeric(zip, "Please enter a valid zip code")){
				if(madeSelection(state, "Please Choose a contract type")){
					if(lengthRestriction(username, 15, 30)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>
</script><script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
	$("#parent_cat").change(function() {
		$(this).after('<div id="loader"><img src="images/loading.gif" alt="loading category" /></div>');
		$.get('faculty-building.php?parent_cat=' + $(this).val(), function(data) {
			$("#sub_cat").html(data);
			$('#loader').slideUp(200, function() {
				$(this).remove();
			});
		});	
    });

});
</script>

</head>

<body>

    <div id="wrapper">
        
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">ADD LOCATIONS</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
             
                          
			                  <form action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>" method="POST" onsubmit='return formValidator()'>
									
									<label for="email">Building:</label>
									<select name="sub_cat" class="form-control" id="sub_cat">
									<?php 
										       $sel_group ="SELECT * FROM  building";
											   $result=$conn->query( $sel_group);
											    
												while($row_group =mysqli_fetch_array($result)){
												
										            
													 
													 echo"<option  value='";
													 echo $row_group ['building_name']; 
													 echo " '>";
													echo $row_group ['building_name']; 
													echo "</option>";
										} ?>
									</select>
									

									<label for="email">Location Name:</label>
									<input name="name"  type="text" class="form-control" id="firstname" required/>

									<label for="email">Location Acronym:</label>
									<input name="acronym"  type="text" class="form-control" id="firstname" required/>

									<label for="email">Location Code:</label>
									<input name="code" type="text"  class="form-control" id="zip" required/>
									<label for ="email">Location Status:</label><br />
								<input name="status" type="radio"  value="Occupied" > Occupied &nbsp;
								<input name="status" type="radio"  value="Not Occupied" > Not Occupied &nbsp;
								<input name="status" type="radio"  value="BOOKED" > BOOKED 
								</br>

									<label for="email">Description:</label>
									<textarea col="2" rows="2" name="description" id="username" class="form-control" ></textarea>
									</br>
									<button type="submit" name="login-btn" onClick ="confirm('Are you sure the location doesn't exist?')"class="btn btn-success">Add Location</button>
									<a href="locations.php"><button type="submit" class="btn btn-primary">Back</button></a>
									</form>
								

                            
                       
    </div>
    <!-- /#wrapper -->

   

</body>
<?php }?>
</html>
