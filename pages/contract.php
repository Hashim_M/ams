<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 


require"config.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>

</head>

<body>

   <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index-1.html">EURS</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
             
                    <a class="navbar-brand"  href="#">
														   <?php
									if (isset($_SESSION["user"]))
									  echo "Welcome " . $_SESSION["user"] . "!";
									else
									  echo "Welcome Admin!";
									?>
                    </a>
					
                  
                 
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a> 
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="profile.php"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a> 
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Admin Management<span class="fa arrow"></span></a>
                             
							 <ul class="nav nav-second-level">
                                <!--<li>
                                    <a href="login.html"><i class="fa fa-sign-in fa-fw"></i> Login</a>
                              </li> -->
							   <li>
                                    <a href="group.php"><i class="fa  fa-angle-right fa-fw"></i> Groups</a>
                              </li>
							   <li>
                                    <a href="category.php"><i class="fa  fa-angle-right fa-fw"></i> Category</a>
                              </li>
							  <li>
                                    <a href="tables.php"><i class="fa  fa-angle-right fa-fw"></i> Assets</a>
                              </li>
							   <li>
                                    <a href="buildings.php"><i class="fa  fa-angle-right fa-fw"></i> Buildings</a>
                              </li>
							  <li>
                                    <a href="locations.php"><i class="fa  fa-angle-right fa-fw"></i> Locations</a>
                              </li>
							  
                               
                            </ul>
							
                            <!-- /.nav-second-level -->
                        </li>
						   <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>ITOC Management<span class="fa arrow"></span></a>
							
							 <ul class="nav nav-second-level">
                              <li>
                                    <a href="affliation.php"><i class="fa  fa-angle-right fa-fw"></i> ITOC Affiliate</a>
                              </li>
							  <li>
                                    <a href="faculty.php"><i class="fa  fa-angle-right fa-fw"></i> Section </a>
                              </li>
							  <li>
                                    <a href="Department.php"><i class="fa  fa-angle-right fa-fw"></i> Department</a>
                              </li>
							 </ul>
                            
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>Requisition Management<span class="fa arrow"></span></a>
							
							 <ul class="nav nav-second-level">
                               <li>
                                    <a href="view_request.php"><i class="fa  fa-angle-right fa-fw"></i> Request<span class="fa arrow"></span></a>
                              </li>
							    <li>
                                    <a href="request-type.php"><i class="fa  fa-angle-right fa-fw"></i> Request Type<span class="fa arrow"></span></a>
                              </li>
							 </ul>
                            
                        </li>
						<li>
                            <a href="#"><i class="fa fa-th fa-fw"></i> Contract Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
							<li>
                                    <a href="contract.php"><i class="fa  fa-angle-right fa-fw"></i>Contractors<span class="fa arrow"></span></a>
                              </li>
							 </ul>
                        </li>
						<li>
                            <a href="#"><i class="fa fa-suitcase fa-fw"></i> Assignment Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <li>
                                    <a href="view-assignment.php"><i class="fa  fa-angle-right fa-fw"></i> Assign<span class="fa arrow"></span></a>
                              </li>
							   <li>
                                    <a href="asset-transfer.php"><i class="fa  fa-angle-right fa-fw"></i> Asset Movement<span class="fa arrow"></span></a>
                              </li>
							 </ul>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> About Us<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <li>
                                    <a href="#">0706 805 838</a>
                              </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                    <h1 class="page-header">Contractors</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List of Contractors 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
							
                           <table class="table table-bordered table-striped" id="dataTables-example">
			               <thead>
				           <tr>
						     <th><input type="checkbox" id="select-all" /></th>
                          
							 <th>Contractor</th>
							
							 <th>Description</th>
							 <th>Date of Contract</th>
							 <th>Edit Action</th>
							  <th>Delete Action</th>
                             
                             
					
						   </tr>
				           </thead>
				  
				           
			                <tbody>
				        <?php  
						 $res = "SELECT * FROM  contract_details";
							
							$result = mysqli_query($conn, $res);

							if (mysqli_num_rows($result) > 0) {
								// output data of each row
								while($row_res = mysqli_fetch_assoc($result)) {?>
						
						
						
							 <tr>
						     <th><input type="checkbox" name="checkbox[]" value="<?php echo $row_res['contract_id'];?>" id="select-all2" /></th>
							
							
							<th><a href="sql_table_to_pdf/company-report.php?edit=<?php echo $row_res['contract_id'];?>">
							<?php echo $row_res['company'];?></a></th>
							
							<th><?php echo $row_res['description'];?></th>
							<th><?php echo $row_res['date_modified'];?></th>
							 <th><a rel="facebox" href="edit-contract.php?edit=<?php echo $row_res['contract_id'];?>"><i class="fa fa-edit fa-fw"></i>Edit </a></td>
          
                             <th><a rel="facebox" href="del-contract.php?del=<?php echo $row_res['contract_id'];?>"><i class="fa fa-trash-o fa-fw"></i>Delete </a></td>
						
						</tr>
							 
							<?php }}
							$conn->close();?>
						   
						   
				            </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="well">
							<P><a rel="facebox" href="add_contract.php"><i class="fa fa-plus fa-fw"></i>Add contractor</a>
		&nbsp; &nbsp; <a href="sql_table_to_pdf/contract-report.php"><i class="fa fa-floppy-o fa-fw"></i>View Reports</a>
		&nbsp; &nbsp; <a  rel="facebox" href="import-contract.php"><i class="fa fa-upload fa-fw"></i>Import Data</a></P>
							</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>
<?php }?>
</html>
