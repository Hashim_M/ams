<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="stylesheet/template.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet/bootstrap.min.css"/>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AssetManager</title>
<script>
function validateForm(){
		   
		   var x = document.forms["form"]["user"].value;
		   var atpos = x.indexOf("@");
		   var dotpos = x.lastIndexOf(".");
		   
		   if(atpos<1 || dotpos<atpos+2 ||dotpos+2>=x.length){
			   
			   alert("the email entered is invalid");
			   return false;
		   }
		   
		   var y = document.forms["form"]["pass"].value;
		   var maxchar= y.length;
		   if(maxchar<8 || maxchar>11){
			   alert("the password characters aren't enough. enter valid \'id\'");
			   return false;
		   }
		  
	   }
	</script>
</head>

<body>
<div id="body">
<div id="container">
	<div id="header">
    	<div id="header-title">UARS</div>
    </div>
    <div id="content">
    	<div id="sidebar">
       
          
      
         <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Administrator Management
    <span class="caret"></span></button>
    <ul class="dropdown-menu">  
        <li class="current"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=login">Login</a></li>          
        <li class="current"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=addAssets">Add Assets</a></li>
         <li class="current"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=search">Item Search</a></li>
        <li class="current"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=home">HOME</a></li>
        <li class="current"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=createMgt">Create Affliation</a></li>
        <li class="current"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=importUser">Import User</a></li>
        <li class="current"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=logs">Logs management</a></li>
      
        <li class="current"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=logout">Logout</a></li>        
      </ul>
  </div>
  </br>
  </br>
  </br>
       
      <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Inventory Management
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a  href="<?php echo $_SERVER['PHP_SELF']; ?>?action=request">Request Management</a></li>
      <li><a  href="<?php echo $_SERVER['PHP_SELF']; ?>?action=report">Report Management</a></li>
      </ul>
  </div>
  </br>
  </br>
  </br>

        
        <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">University Management
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a  href="<?php echo $_SERVER['PHP_SELF']; ?>?action=request">View Departments</a></li>
      <li><a  href="<?php echo $_SERVER['PHP_SELF']; ?>?action=request">View Faculty</a></li>
      <li><a  href="<?php echo $_SERVER['PHP_SELF']; ?>?action=request">View Location</a></li>
    </ul>
  </div>
  </br>
  </br>
  </br>
        </div>
        <div id="main-content">
        
         
      
          <?php 
		 if(isset($_GET['action'])){
			 $action=mysql_real_escape_string(stripslashes(strip_tags($_GET['action'])));
			 switch($action){
				 case "addAssets":
					 
					 break;
				 case "createMgt":
					 echo "Create Management!";
					 break;
				 case "importUser":
					 echo "Import User!";
					 break;
				 case "exportUser":
					 echo "Export User!";
					 break;
				 case "search":
					 require_once("search.php");
					 break;
					 
				 case "logs":
					 echo "logs management";
					 break;
				  
				 case "logout":
					 echo "Session timeout";
					 break;
				 case "request":
					 echo "Request Management";
					 require_once('view_request.php');
					 break;
				case "Report":
					 echo "Report Management";
					 break;
					 
					 //login module
					 
				case "login":
					echo 'login module';
					require_once('login.php');
              
					
					break;
			 }
			 
		 }		  
		  
		  
		  ?>   
                
        </div>
    </div>
    <div id="footer">Copyright &copy;2015. All Rights Reserved EU.</div>
</div>
</div>

</body>
</html>