<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require'config.php';
  
	
	 
	if(isset($_GET['edit'])){
		
		$edit_id = $_GET['edit'];
		
		
		 $edit_query="SELECT * FROM item_allocations WHERE allocation_id='$edit_id ' LIMIT 1";
	 
	 $run_edit = $conn->query($edit_query);
	 
	 while($edit_row=mysqli_fetch_array($run_edit)){ 
		
		 
		
	
	  
  ?>
			
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
  <script type="text/javascript">
 

function formValidator(){
	// Make quick references to our fields
	var name = document.getElementById('firstname');
	var zip = document.getElementById('zip');
	var code = document.getElementById('t-code');
	var state = document.getElementById('state');
	var description = document.getElementById('description');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(name, "Please enter only letters for your name")){
		if(isAlphanumeric(zip, "Numbers and Letters Only")){
			if(isNumeric(code, "Please enter a valid tracking code")){
				if(madeSelection(state, "Please Choose approriate selection")){
					if(lengthRestriction(description, 6, 8)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>

</head>

<body>

    <div id="wrapper">
     
                <div class="row">
                    <div class="col-lg-12">
                      <h3 class="page-header">EDIT ALLOCATION RECORDS</h3>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
             
							
						 <form onsubmit="return formValidator()" action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>?edit_form=<?php echo $edit_row['allocation_id'];?>" method= "post">
								
						   <label for ="group_name">ASSIGNEE</label>
						   <input type="text" name="assignee" id="firstname" class="form-control" value="<?php echo $edit_row['assignee'];?>" size="40" required/>
							
						 <label for="item">ITEM:</label>
							 <select class="form-control" name="item" id="state">
							 <option value="<?php echo $edit_row['item_id'];?>"><?php echo $edit_row['item_id'];?></option>
							 
							 <?php 
										       $sel_item = "SELECT * FROM items ";
											   $res = $conn->query($sel_item);
											    
												while($row_group = mysqli_fetch_array($res)){
												
										            
													 
													 echo"<option  value='";
													 echo $row_group ['item']; 
													 echo " '>";
													echo $row_group ['item']; 
													echo "</option>";
										} ?>
							 </select>
							 
							 </br>
							 <label for ="group_name">Return Date</label>
						 
								  	 <select name="item" >
									 <option value="day"> Day </value>
									 <?php 
									 $count = 1;
									 for( $count=1; $count<=31; ++$count){
									 
									     echo"<option  value='";
													 echo $count; 
													 echo " '>";
													echo $count; 
													echo "</option>";
									 
									 }
									 
									 ?>
									 </select>
									 &nbsp;&nbsp;&nbsp;&nbsp;
									  <select name="item1"  >
									  <option value="months">Months</option>
									 <?php 
									 $count = 1;
									 for( $count=1; $count<=12; ++$count){
									 
									     echo"<option  value='";
													 echo $count; 
													 echo " '>";
													echo $count; 
													echo "</option>";
									 
									 }
									 
									 ?>
									 </select>
									 &nbsp;&nbsp;&nbsp;&nbsp;
									  <select name="item2"  >
									  <option value="Year">Year</option>
									 <?php 
									 $count = 2015;
									 for( $count=2015; $count<=2030; ++$count){
									 
									     echo"<option  value='";
													 echo $count; 
													 echo " '>";
													echo $count; 
													echo "</option>";
									 
									 }
									 
									 ?>
									 </select>
							</br>
							 
							 
						   
						  <button type="submit" name="submit" onClick="confirm('Are you sure, you want to save the update')" class="btn btn-success">Update</button></h1>
						  <button type="reset" name="submit"  class="btn btn-danger">Reset</button></h1> 		
					</form>
					<?php }}?>

					<?php
						
						 if(isset($_POST['submit']))
						  {
						 global $item_id;
						$edit_id =  mysqli_real_escape_string($conn,$_GET['edit_form']); 
						$item =  mysqli_real_escape_string($conn,$_POST['item']);
					
						$return_date =  mysqli_real_escape_string($conn,$_POST['return_date']);	
						$assigne =  mysqli_real_escape_string($conn,$_POST['assignee']);
						
							
							
							$sel_item_id = "SELECT item_id FROM items WHERE item LIKE '% $item %' LIMIT 1";
							$reslt= $conn->query($sel_item_id);
								 while($row_data =mysqli_fetch_array($reslt )){
								 
								 $item_id = $row_data ['item_id'];
											 
								 
								 }
						
						

							$update_query ="UPDATE `item_allocations` SET `item_id`='$item_id ',`user`=' admin',assignee='$assigne',date_of_allocation=now(), 
										   date_of_return ='$return_date' WHERE allocation_id ='$edit_id'";
							
							$res= $conn->query($update_query) or die(mysqli_error($conn));

						
							
							echo "<script>alert('Allocation is Successfully updated.')</script>";
							echo "<script>window.open('view-assignment.php','_self')</script>";
							
							
						  }
						
						?>
	 
    </div>
    <!-- /#wrapper -->

    

</body>
<?php } ?>
</html>
