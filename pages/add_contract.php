	<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require'config.php';
     if(isset($_POST['submit'])){
	   
	   $company =  mysqli_real_escape_string($conn,$_POST['company']);
	   $email = mysqli_real_escape_string($conn,$_POST['email']);
	   $telephone =  mysqli_real_escape_string($conn,$_POST['telephone']);
	   $box =  mysqli_real_escape_string($conn,$_POST['box-no']);
	   $postal=  mysqli_real_escape_string($conn,$_POST['postal']);
	   $contract =  mysqli_real_escape_string($conn,$_POST['contract-type']);
	   $item = mysqli_real_escape_string($conn,$_POST['item']);
	   $city =  mysqli_real_escape_string($conn,$_POST['city']);
	   $contract_desc =  mysqli_real_escape_string($conn,$_POST['contract-desc']);
		 
		
		 
		 
		 $insert_query = "INSERT INTO contract_details(company,email,telephone,po_box,postal_code,location,contract_type,item,description,date_modified)
		 VALUE('$company ','$email','$telephone',' $box',' $postal','$city','$contract','$item',' $contract_desc',now())";
		 
		 if($conn->query($insert_query)=== true)
		 
		{
		echo "<script>alert('Contractor  is Successfully added.')</script>";
		echo "<script>window.open('contract.php','_self')</script>";
		}
		else
		{
			die(mysqli_error($conn));
		}
		 
		 }
		
		
		

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  <script type="text/javascript">
  

function formValidator(){
	// Make quick references to our fields
	var firstname = document.getElementById('firstname');
	var addr = document.getElementById('addr');
	var zip = document.getElementById('zip');
	var state = document.getElementById('state');
	var username = document.getElementById('username');
	var email = document.getElementById('email');
	var phone =document.getElementById('phone')
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(firstname, "Please enter only letters for your name")){
		if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
			if(isNumeric(zip, "Please enter a valid zip code")){
				if(madeSelection(state, "Please Choose a contract type")){
					if(lengthRestriction(username, 15, 30)){
						if(emailValidator(email, "Please enter a valid email address")){
						  if(phonenumValidator(phone, "Please enter a valid phone number")){
							return true;
							}
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function phonenumValidator(elem,helperMsg){
    var phonenumExp = /^(\()?[2-9]{1}\d{2}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/;
	if(elem.value.match(phonenumExp)){
	
	 return true;
	
	}else{
	 alert(helpMsg);
	 elem.focus();
	 return false;
	
	}




}
</script>

  
  
 

</head>

<body>

    <div id="wrapper">
  
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">ADD CONTRACT</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
                
                          
			                      <form action="<?php echo htmlspecialchars($_SERVER[ 'PHP_SELF']);?>" method= "post" enctype="multipart/form-data" onsubmit="return formValidator()">
									
									   <label for ="company-name">COMPANY NAME</label>
									   <input type="text" name="company" size="40" id="firstname" class="form-control" required/>
										
										<label for ="email">EMAIL ADDRESS</label>
									   <input type="text" name="email" size="40" id= "email" class="form-control" required/>
										
									  <label for ="telephone">TELEPHONE NO</label>
									   <input type="text" name="telephone" size="40" id="phone"class="form-control" required/>
									
										<label for ="box-no">P.O BOX</label>
									   <input type="text" name="box-no" size="40" id="addr" class="form-control" required/>
										
										<label for ="postal">POSTAL CODE</label>
									   <input type="text" name="postal" size="40" id="zip" class="form-control" required/>
										
										<label for ="location">CITY</label>
									   <input type="text" name="city" size="40" id="firstname" class="form-control" required/>
										
										
										<label for="contract-type">CONTRACT TYPE:</label>
										 <select class="form-control" name="contract-type" id="state">
										 <option value="GENERAL">GENERAL</option>
										 <option value="SPECIFIC">SPECIFIC</option>
										 </select> 
										  </br>		  
									   <label for ="asset_name">ITEM SUPPLIED</label>
									   <input type="text" name="item" size="40" id="firstname" class="form-control" required/>
										
									   
									   <label for ="category_description">CONTRACT DESCRIPTION</label>
									   <textarea  colspan="2" rowspan="2" name="contract-desc"  class="form-control"></textarea>
									
										
									   
									  <button type="submit" name="submit" class="btn btn-success">Add Contract</button></h1>
									  <button type="reset" name="submit"  class="btn btn-danger">Reset</button></h1>
									   
									 </form>



                   

    </div>
    <!-- /#wrapper -->

   
</body>
<?php }?>
</html>
