-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2016 at 08:36 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `affliate_department`
--

CREATE TABLE IF NOT EXISTS `affliate_department` (
  `affln_depart_id` int(10) NOT NULL AUTO_INCREMENT,
  `affln_depart_name` varchar(55) DEFAULT NULL,
  `affln_id` int(12) DEFAULT NULL,
  `affln_depart_code` varchar(10) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`affln_depart_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `affliate_department`
--

INSERT INTO `affliate_department` (`affln_depart_id`, `affln_depart_name`, `affln_id`, `affln_depart_code`, `date_modified`) VALUES
(1, 'COMMUNITYDEVELOPT', 0, 'EUF001D001', '2016-03-21 09:42:28'),
(2, 'COMPSCI', 0, 'EUF004D001', '2016-03-21 09:42:51'),
(3, 'ECONSTAT', 5, 'EUF005D001', '2016-03-21 09:43:20'),
(4, 'ANHE', 3, 'EUF002D001', '2016-03-21 09:44:53'),
(5, 'ENSCI', 6, 'EUF006D001', '2016-03-21 09:45:36'),
(6, 'GENDER', 7, 'EUF007D001', '2016-03-21 09:46:46'),
(7, 'TRANSPORT', 8, 'EUF003D001', '2016-03-21 09:51:57'),
(8, 'CATERING ', 8, 'EUF007D002', '2016-03-21 09:52:48'),
(9, 'GUIDANCEANDCOUNSELLING', 8, 'EUF007D003', '2016-03-21 09:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `affliate_faculty`
--

CREATE TABLE IF NOT EXISTS `affliate_faculty` (
  `affln_id` int(12) NOT NULL AUTO_INCREMENT,
  `affln_name` varchar(50) DEFAULT NULL,
  `affln_code` varchar(12) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`affln_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=9 ;

--
-- Dumping data for table `affliate_faculty`
--

INSERT INTO `affliate_faculty` (`affln_id`, `affln_name`, `affln_code`, `date_modified`) VALUES
(1, 'FEDCOS', 'EUF001', '2016-03-21 09:30:21'),
(3, 'AGRICULTURE', 'EUF002', '2016-03-21 09:30:43'),
(4, 'SCIENCE', 'EUF003', '2016-03-21 09:31:07'),
(5, 'FASS', 'EUF004', '2016-03-21 09:31:28'),
(6, 'FERD', 'EUF005', '2016-03-21 09:31:52'),
(7, 'GENDER', 'EUF006', '2016-03-21 09:32:20'),
(8, 'OTHER', 'EUF007', '2016-03-21 09:51:21');

-- --------------------------------------------------------

--
-- Table structure for table `affliation`
--

CREATE TABLE IF NOT EXISTS `affliation` (
  `affliate_id` int(12) NOT NULL AUTO_INCREMENT,
  `affliate_name` varchar(55) DEFAULT NULL,
  `affliate_code` varchar(25) DEFAULT NULL,
  `affliate_description` varchar(255) DEFAULT NULL,
  `founding_date` date DEFAULT NULL,
  PRIMARY KEY (`affliate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `affliation`
--

INSERT INTO `affliation` (`affliate_id`, `affliate_name`, `affliate_code`, `affliate_description`, `founding_date`) VALUES
(1, 'ADMINISTRATION', 'EU001 ', 'ADMINISTRATIVE  AND MANAGEMENT  ROLE OF THE EU ', '2016-03-21'),
(2, 'FACULTY', 'EU002', 'HIGHEST ACADEMIC LEVEL IN THE EU', '2016-03-21'),
(3, 'DEPARTMENT', 'EU003', 'ALL THE DEPARTMENTAL LEVEL IN THE ENTIRE EU', '2016-03-21');

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE IF NOT EXISTS `building` (
  `building_id` int(12) NOT NULL AUTO_INCREMENT,
  `building_code` varchar(20) DEFAULT NULL,
  `building_name` varchar(40) DEFAULT NULL,
  `affln_id` int(12) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `date_of_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`building_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`building_id`, `building_code`, `building_name`, `affln_id`, `status`, `date_of_modification`) VALUES
(1, 'EUCOMPLX001', 'EDUCATIONCOMPLEX', 1, 'Good', '2016-03-21 08:55:43'),
(2, 'EUCOMPLX002', 'SCIENCECOMPLEX', 4, 'GOOD', '2016-03-21 09:48:50'),
(3, 'EUB003', 'FASSBLOCKA', 5, 'GOOD', '2016-03-21 09:49:49'),
(4, 'EUB004', 'FASSBLOCKB', 5, 'GOOD', '2016-03-21 09:50:17'),
(5, 'EUB005', 'TRANSPORTSTORE', 8, 'GOOD', '2016-03-21 09:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `contract_details`
--

CREATE TABLE IF NOT EXISTS `contract_details` (
  `contract_id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(55) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `telephone` int(14) DEFAULT NULL,
  `po_box` int(14) DEFAULT NULL,
  `postal_code` int(14) DEFAULT NULL,
  `location` varchar(25) DEFAULT NULL,
  `contract_type` varchar(25) DEFAULT NULL,
  `item` varchar(25) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`contract_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contract_details`
--

INSERT INTO `contract_details` (`contract_id`, `company`, `email`, `telephone`, `po_box`, `postal_code`, `location`, `contract_type`, `item`, `description`, `date_modified`) VALUES
(1, ' comptech', ' comptech@info.com', 2147483647, 248, 20016, 'KISUMU', ' ', ' COMPUTERS', '  computers and general ', '2016-03-21 13:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `depreciation`
--

CREATE TABLE IF NOT EXISTS `depreciation` (
  `depreciation_id` int(12) NOT NULL AUTO_INCREMENT,
  `depreciation_type` varchar(255) DEFAULT NULL,
  `depreciation_rate` double(12,0) DEFAULT NULL,
  `depreciation_formula` varchar(20) NOT NULL,
  `depreciation_description` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`depreciation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `depreciation`
--

INSERT INTO `depreciation` (`depreciation_id`, `depreciation_type`, `depreciation_rate`, `depreciation_formula`, `depreciation_description`, `date_modified`) VALUES
(1, 'straightline', 20, '0', 'for calculating solvent assets', '2016-03-05 09:42:35');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `item_id` int(12) NOT NULL AUTO_INCREMENT,
  `qty` int(255) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `modified_by` int(12) DEFAULT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`item_id`, `qty`, `status`, `modified_by`, `date_modified`) VALUES
(1, 2, 'GOOD', 0, '2016-03-21 09:58:30'),
(2, 1, 'GOOD', 0, '2016-03-21 09:59:50'),
(3, 1, 'GOOD', 0, '2016-03-21 10:08:05'),
(4, 1, 'GOOD', 0, '2016-03-21 10:09:51'),
(5, 1, 'GOOD', 0, '2016-03-21 15:43:15'),
(6, 2, 'GOOD', 0, '2016-03-21 15:48:35');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(15) NOT NULL AUTO_INCREMENT,
  `item` varchar(20) DEFAULT NULL,
  `group_id` int(15) DEFAULT NULL,
  `cat_id` int(15) DEFAULT NULL,
  `loc_id` int(12) DEFAULT NULL,
  `serial_number` int(32) DEFAULT NULL,
  `qty` int(30) DEFAULT '0',
  `default_value` float(12,0) DEFAULT NULL,
  `current_value` float(12,0) DEFAULT NULL,
  `depreciated_value` float(12,0) DEFAULT NULL,
  `warranty_duration` varchar(10) DEFAULT NULL,
  `item_description` varchar(250) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item`, `group_id`, `cat_id`, `loc_id`, `serial_number`, `qty`, `default_value`, `current_value`, `depreciated_value`, `warranty_duration`, `item_description`, `status`, `date_modified`) VALUES
(1, 'TRUCK', 1, 1, 2, 2147483647, 1, 120000000, 0, 0, '2', 'for ferrying bulky and heavy luggages', 'GOOD', '2016-03-21 07:09:30'),
(2, 'TRUCK', 1, 1, 2, 2147483647, 1, 120000000, 0, 0, '2', 'for ferrying bulky and heavy luggages', 'GOOD', '2016-03-21 07:09:47'),
(3, 'TRUCK', 1, 1, 2, 2147483647, 1, 120000000, 0, 0, '2', 'for ferrying bulky and heavy luggages', 'GOOD', '2016-03-21 07:09:50'),
(4, 'TRUCK', 1, 1, 2, 2147483647, 1, 120000000, 0, 0, '2', 'for ferrying bulky and heavy luggages', 'GOOD', '2016-03-21 07:09:51'),
(5, 'TRUCK', 1, 1, 2, 2147483647, 1, 2000000000, 0, 0, '2years', 'for ferrying bulky luggagess', 'GOOD', '2016-03-21 12:43:15'),
(6, 'TRUCK', 1, 1, 2, 2147483647, 2, 90000000, 0, 0, '2years', 'truck to carry heavy luggage', 'GOOD', '2016-03-21 12:48:34'),
(7, 'TRUCKS', 1, 1, 2, 2147483647, 2, 40000000000, 0, 0, '2years', 'TRUCKS FOR FERRYING HEAVY TRUCKS', 'GOOD', '2016-03-21 12:50:53');

-- --------------------------------------------------------

--
-- Table structure for table `item_allocations`
--

CREATE TABLE IF NOT EXISTS `item_allocations` (
  `allocation_id` int(10) NOT NULL,
  `item_id` int(10) DEFAULT NULL,
  `user` varchar(10) DEFAULT NULL,
  `assignee` varchar(255) DEFAULT NULL,
  `date_of_allocation` date DEFAULT NULL,
  `date_of_return` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_categories`
--

CREATE TABLE IF NOT EXISTS `item_categories` (
  `cat_id` int(10) NOT NULL AUTO_INCREMENT,
  `item_group_id` int(10) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `item_cat_description` varchar(255) DEFAULT NULL,
  `date_of_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `item_group_id` (`item_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `item_categories`
--

INSERT INTO `item_categories` (`cat_id`, `item_group_id`, `category`, `item_cat_description`, `date_of_modification`) VALUES
(1, 0, 'TABLES', 'ALL THE TABLES OWNED BY EGER UNIVERSITY', '2016-03-21 08:48:36'),
(2, 2, 'HEAVYCOMMERCIAL', 'ALL HEAVY TRUCKS OWNED BY THE EU', '2016-03-21 08:43:45'),
(3, 3, 'COMBINEHARVESTER', 'ALL THE COMBINE HARVESTERS POSSESSED', '2016-03-21 08:45:58');

-- --------------------------------------------------------

--
-- Table structure for table `item_group`
--

CREATE TABLE IF NOT EXISTS `item_group` (
  `item_group_id` int(10) NOT NULL AUTO_INCREMENT,
  `item_group_name` varchar(55) DEFAULT NULL,
  `item_group_desc` varchar(255) DEFAULT NULL,
  `date_of_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`item_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `item_group`
--

INSERT INTO `item_group` (`item_group_id`, `item_group_name`, `item_group_desc`, `date_of_modification`) VALUES
(1, 'FURNITURE ', 'ALL FURNITURES IN EGERTON UNIVERSITY', '2016-03-21 08:20:56'),
(2, 'MOTORVEHICLES', 'ALL VEHICLES OWNED BY THE EU', '2016-03-21 08:18:34'),
(3, 'FARMMACHINES', 'ALL FARM MACHINERIES OWNED BY THE EU', '2016-03-21 08:19:15');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `loc_id` int(12) NOT NULL AUTO_INCREMENT,
  `loc_code` varchar(12) DEFAULT NULL,
  `loc_acronym` varchar(20) DEFAULT NULL,
  `loc_name` varchar(50) DEFAULT NULL,
  `affln_id` int(12) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_modified` date DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `building_id` int(12) DEFAULT NULL,
  PRIMARY KEY (`loc_id`),
  KEY `affln_id` (`affln_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`loc_id`, `loc_code`, `loc_acronym`, `loc_name`, `affln_id`, `description`, `date_modified`, `status`, `building_id`) VALUES
(2, 'COMPLX001001', 'ET1', 'EDUCATIONTHEATRE', 1, 'LECTURE HALL AND CONFERENCE ROOM FOR THE UNIVERSITY', '2016-03-21', 'Not Occupied', 0),
(3, 'EUCOMPLX0010', 'ET2', 'EDUCATIONTHEATRE', 1, 'LECTURE HALL AND CONFERENCE CENTER', '2016-03-21', 'Occupied', 0),
(4, 'EUBL001', 'TS', 'TRUCKSTORE', 8, 'FOR STORING ALL HEAVY COMMERCIAL TRUCKS', '2016-03-21', 'Occupied', 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_user`
--

CREATE TABLE IF NOT EXISTS `login_user` (
  `user_id` int(13) NOT NULL DEFAULT '0',
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `login_attempt` int(10) DEFAULT NULL,
  `permission` int(10) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_user`
--

INSERT INTO `login_user` (`user_id`, `username`, `password`, `login_attempt`, `permission`) VALUES
(0, 'dan@gmail.com', 'dan', 0, 1),
(1, 'admin@gmail.com', 'admin', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `remote_addr` varchar(128) DEFAULT NULL,
  `request_uri` varchar(255) DEFAULT NULL,
  `referrer` varchar(12) DEFAULT NULL,
  `useragent` varchar(10) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance`
--

CREATE TABLE IF NOT EXISTS `maintenance` (
  `maintenance_id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) DEFAULT NULL,
  `dateOfMaintenance` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`maintenance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `request_id` int(10) NOT NULL AUTO_INCREMENT,
  `requester` int(10) DEFAULT NULL,
  `req_type` varchar(8) DEFAULT NULL,
  `item` varchar(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_submitted` datetime DEFAULT NULL,
  `approved_by` int(10) DEFAULT NULL,
  `date_approved` datetime DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `item_id` (`item`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`request_id`, `requester`, `req_type`, `item`, `description`, `date_submitted`, `approved_by`, `date_approved`, `status`) VALUES
(1, 0, '', 'vehicle', 'for ferrying staff from the residential areas', '2016-03-22 12:47:26', 0, '2016-03-22 12:47:49', ' yes');

-- --------------------------------------------------------

--
-- Table structure for table `request_type`
--

CREATE TABLE IF NOT EXISTS `request_type` (
  `request_type_id` int(12) NOT NULL AUTO_INCREMENT,
  `request_type` varchar(30) DEFAULT NULL,
  `request_type_description` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`request_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `request_type`
--

INSERT INTO `request_type` (`request_type_id`, `request_type`, `request_type_description`, `date_modified`) VALUES
(1, 'General', 'meant for the group of persons or meant for every employee', '2016-03-22 12:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `university_hierarchy`
--

CREATE TABLE IF NOT EXISTS `university_hierarchy` (
  `h_level_id` int(20) NOT NULL AUTO_INCREMENT,
  `h_level_name` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`h_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `home_phone` int(10) DEFAULT NULL,
  `cell_phone` int(10) DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `permission` int(10) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `password`, `date_modified`, `dob`, `home_phone`, `cell_phone`, `gender`, `permission`) VALUES
(2, ' dancun', ' ogindo', 'dan@gmail.com', 'dan', '2016-02-22 05:21:01', '0000-00-00 00:00:00', 54657890, 9897654, 'Male ', NULL),
(3, ' james', ' karimu', 'karimu@gmail.com', 'user', '2016-03-05 05:02:07', '1994-02-21 00:00:00', 20756438, 724563421, 'Male ', 2),
(5, '\0', '\0', 'admin@gmail.com', 'admin', '2016-03-19 21:00:00', '1993-06-20 00:00:00', 708426668, 787689090, 'male', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
