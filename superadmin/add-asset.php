<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{
require'config.php';	
  
       if(isset($_GET['submit'])){
	    global $item_id;
	    global $item_group_id;
	    global $cat_id;
		global $loc_id;
	    $group = mysqli_real_escape_string($conn,$_GET['parent_cat']);
	    $category = mysqli_real_escape_string($conn,$_GET['sub_cat']);
	    $location = mysqli_real_escape_string($conn,$_GET['location']);
		$asset = mysqli_real_escape_string($conn,$_GET['asset']);
		$sr_code = mysqli_real_escape_string($conn,$_GET['sr-code']);
	    $asset_warranty = mysqli_real_escape_string($conn,$_GET['asset-warranty']);
		$asset_desc =  mysqli_real_escape_string($conn,$_GET['asset-desc']);
		$status= mysqli_real_escape_string($conn,$_GET['status']);
		$qty = mysqli_real_escape_string($conn,$_GET['qty']+0);
		$modifier = $_SESSION['user'];
		
		
	     $select_cat_id= mysqli_query($conn,"SELECT cat_id FROM item_categories WHERE category ='$category'") or die(mysqli_error($conn));
		 
		
		       while($row = mysqli_fetch_array( $select_cat_id)){
			       $cat_id = $row['cat_id'];
				 
				   
			   }
	   $select_group_id= mysqli_query($conn,"SELECT item_group_id FROM item_group WHERE item_group_name =' $group'") or die(mysqli_error($conn));
		 
		
		       while($row = mysqli_fetch_array( $select_group_id)){
			       $item_group_id = $row['item_group_id'];
				 
				   
			   }
		 $select_loc_id = mysqli_query($conn, "SELECT loc_id FROM location WHERE loc_name ='$location' LIMIT 1") or die(mysqli_erro($conn));
		 
		        while($row = mysqli_fetch_array( $select_loc_id )){
			       $loc_id = $row['loc_id'];
				   
				   
			   }
		 
		 $insert_query = mysqli_query($conn,"INSERT INTO items (item,group_id,cat_id,loc_id,serial_number, qty,warranty_duration,item_description,status,date_modified)
		 VALUES('$asset','$item_group_id','$cat_id ',' $loc_id','$sr_code','$qty','$asset_warranty','$asset_desc','$status', now())");
		 
		  $SELECT = mysqli_query($conn, "SELECT item_id FROM items ORDER BY item_id  DESC LIMIT 1 ") or die(mysqli_error($conn));
		       while($row = mysqli_fetch_array($SELECT)){
			       $item_id = $row['item_id'];
			   }
         $insert_into_inventory = mysqli_query($conn, "INSERT INTO inventory(item_id, item, qty, status,modified_by,date_modified) 
		 VALUES ('$item_id','$asset','$qty','$status','$modifier',now())") or die(mysqli_error($conn));		 
				  
		 
		echo "<script>alert('The Assets is Successfully Saved.')</script>";
		echo "<script>window.open('tables.php','_self')</script>";
		
	
	
			
	    
	 }
  
  
  
?>
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  <script type="text/javascript">
 

function formValidator(){
	// Make quick references to our fields
	var name = document.getElementById('asset-name');
	var s-code = document.getElementById('s-code');
	var code = document.getElementById('t-code');
	var state = document.getElementById('state');
	var description = document.getElementById('description');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(name, "Please enter only letters for your name")){
		if(isAlphanumeric(s-code, "Numbers and Letters Only")){
			if(isNumeric(code, "Please enter a valid tracking code")){
				if(madeSelection(state, "Please Choose approriate selection")){
					if(lengthRestriction(description, 6, 8)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z\s]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script><script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
	$("#parent_cat").change(function() {
		$(this).after('<div id="loader"><img src="images/loading.gif" alt="loading category" /></div>');
		$.get('loadsubcat.php?parent_cat=' + $(this).val(), function(data) {
			$("#sub_cat").html(data);
			$('#loader').slideUp(200, function() {
				$(this).remove();
			});
		});	
    });

});
</script>


</head>

<body>

    <div id="wrapper">
        
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">ADD ASSETS</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
              
                          
			                    <form action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>" method= "GET" enctype="multipart/form-data" onsubmit= 'return formValidator()'>
									 
										 <label for="group">ASSET GROUP:</label>
										 <select name="parent_cat" class="form-control"  id="parent_cat">
										  <option value="select">select</option>    
										 <?php 
										       $sel_group ="SELECT * FROM item_group";
											   $result = $conn->query($sel_group);
											    
												while($row_group = mysqli_fetch_array($result)){
												
										       
													 
													 echo"<option  value='";
													 echo $row_group ['item_group_name']; 
													 echo " '>";
													echo $row_group ['item_group_name']; 
													echo "</option>";
										} ?>
										 </select>
									 
									 <label for="category">ASSET CATEGORY:</label>
										 <select class="form-control" name="sub_cat"  id="sub_cat"></select>
										 
									  <label for="location">LOCATION:</label>
									   <select class="form-control" name="location" id="state">
									     <option selected value='.\select\.'>..\select\..</option>
										  <?php 
										       $sel_group ="SELECT * FROM location";
											    $group_result = $conn->query($sel_group);
												while($row_group = mysqli_fetch_array($group_result)){
													 echo"<option  value='";
													 echo $row_group ['loc_name']; 
													 echo " '>";
													echo $row_group ['loc_name']; 
													echo "</option>";
										} ?>
										 </select> 
										
										    
									   <label for ="asset_name">ASSET NAME</label>
									   <input type="text" name="asset" size="40" class="form-control" id="asset-name" required/>
									
									   
										 <label for ="asset_name">SERIAL NUMBER</label>
									   <input type="text" name="sr-code" id="s-code" size="40" class="form-control" required/>
										
									
										<label for ="asset value">QUANTITY</label>
							          <input type="text" name="qty" size="40" id="t-code" class="form-control"  required/>
								
										 
									   
									   <label for ="warranty-duration">WARRANTY DURATION IN DAYS</label>
									   <input type="date" name="asset-warranty" size="40" class="form-control" id="s-code" required/>
										</br>
									   
									   <label for ="category_description">ASSET DESCRIPTION</label>
									   <textarea  colspan="5" rowspan="2" name="asset-desc" id= "description" class="form-control"></textarea>
									
										
									   <label for ="status">ASSET STATUS</label>
									   <input type="radio" name="status" size="40"  value="GOOD" required/> GOOD &nbsp;
									   <input type="radio" name="status" size="40"  value="FAULTY" required/> FAULTY
										
									   
									  <button type="submit" name="submit" class="btn btn-success">ADD ASSET</button></h1>
									  <button type="reset" name="submit"  class="btn btn-danger">RESET</button></h1>
									   
									 </form>
								


    </div>
    <!-- /#wrapper -->


</body>
<?php }?>
</html>
