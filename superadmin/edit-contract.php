<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require'config.php';
	 if(isset($_GET['edit'])){
		
	 $edit_id = $_GET['edit'];
			
	 $edit_query="SELECT * FROM contract_details WHERE contract_id ='$edit_id ' LIMIT 1";
	 
	 $run_edit = $conn->query($edit_query);
	 
	 while($edit_row =mysqli_fetch_array($run_edit)){ 
		
				?>
				
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
 <script type="text/javascript">
  

function formValidator(){
	// Make quick references to our fields
	var firstname = document.getElementById('firstname');
	var addr = document.getElementById('addr');
	var zip = document.getElementById('zip');
	var state = document.getElementById('state');
	var username = document.getElementById('username');
	var email = document.getElementById('email');
	var phone =document.getElementById('phone')
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(firstname, "Please enter only letters for your name")){
		if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
			if(isNumeric(zip, "Please enter a valid zip code")){
				if(madeSelection(state, "Please Choose a contract type")){
					if(lengthRestriction(username, 15, 30)){
						if(emailValidator(email, "Please enter a valid email address")){
						  if(phonenumValidator(phone, "Please enter a valid phone number")){
							return true;
							}
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function phonenumValidator(elem,helperMsg){
    var phonenumExp = /^(\()?[2-9]{1}\d{2}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/;
	if(elem.value.match(phonenumExp)){
	
	 return true;
	
	}else{
	 alert(helpMsg);
	 elem.focus();
	 return false;
	
	}




}
</script>


</head>

<body>

    <div id="wrapper">
      
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">EDIT CONTRACTS</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
               
						
							 <form  action="edit-contract.php?edit_form=<?php echo $edit_row['contract_id'];?>" method="post" onsubmit="return formValidator()">
							
							   <label for ="company-name">COMPANY NAME</label>
							   <input type="text" name="company" id="firstname" value="<?php echo $edit_row['company']; ?>" size="40" class="form-control" required/>
								
								<label for ="email">EMAIL ADDRESS</label>
							   <input type="text" name="email" size="40" id="email" value="<?php echo $edit_row['email']; ?>"  class="form-control" required/>
								</br>
							  <label for ="telephone">TELEPHONE NO</label>
							   <input type="text" name="telephone" size="40" id="phone" value="<?php echo $edit_row['telephone']; ?>"  class="form-control" required/>
								
								<label for ="box-no">P.O BOX</label>
							   <input type="text" name="box-no" size="40" id="addr" value="<?php echo $edit_row['po_box']; ?>"  class="form-control" required/>
								
								<label for ="postal">POSTAL CODE</label>
							   <input type="text" name="postal" size="40" id="zip" value="<?php echo $edit_row['postal_code'] ;?>"  class="form-control" required/>
								
								<label for ="location">CITY</label>
							   <input type="text" name="city" size="40" id="firstname" value="<?php echo $edit_row['location']; ?>"  class="form-control" required/>
								
								<label for="contract-type">CONTRACT TYPE:</label>
								 <select class="form-control" name="contract-type" id="state">
								 <option value selected="GENERAL"><?php echo $edit_row['contract_type']; ?> </option>
								 <option value="SPECIFIC">SPECIFIC</option>
								 </select> 
									  
							   <label for ="asset_name">ITEM SUPPLIED</label>
							   <input type="text" name="item" id="firstname" size="40" value="<?php echo $edit_row['item']; ?>"  class="form-control" required/>
								
							   
							   <label for ="category_description">CONTRACT DESCRIPTION</label>
							   <textarea  colspan="2" rowspan="2" name="contract-desc"  value=""  class="form-control"><?php echo $edit_row['description']; ?></textarea>
							
								
							   
							  <button type="submit" name="submit" onClick="confirm('Are you sure, you want to save the updates')" class="btn btn-success">Add Contract</button></h1>
							  <button type="reset" name="submit"  class="btn btn-danger">Reset</button></h1>
							   
							 </form>

						<?php }}?>

						<?php
							 if(isset($_POST['submit']))
							  {
							   $edit_id = mysqli_real_escape_string($conn,$_GET['edit_form']);
							   $company =  mysqli_real_escape_string($conn,$_POST['company']);
							   $email =  mysqli_real_escape_string($conn,$_POST['email']);
							   $telephone =  mysqli_real_escape_string($conn,$_POST['telephone']);
							   $box =  mysqli_real_escape_string($conn,$_POST['box-no']);
							   $postal=  mysqli_real_escape_string($conn,$_POST['postal']);
							   $contract = mysqli_real_escape_string($conn,$_POST['contract-type']);
							   $item =  mysqli_real_escape_string($conn,$_POST['item']);
							   $city =  mysqli_real_escape_string($conn,$_POST['city']);
							   $contract_desc =  mysqli_real_escape_string($conn,$_POST['contract-desc']);
							
							$update_query ="UPDATE `contract_details` SET `company`=' $company',`email`=' $email',`telephone`='$telephone',po_box=' $box ',postal_code= '$postal',
								location='$city', contract_type=' $contract', item=' $item', description=' $contract_desc',`date_modified`=now() WHERE contract_id ='$edit_id'";
								   
								   if($conn->query($update_query) === TRUE)
								{
								echo "<script>alert('The contract is Successfully Updated.')</script>";
								echo "<script>window.open('contract.php','_self')</script>";
								}
								else
								{
									echo $conn->error;
								}
							
							 
							
									
									}?>
			

    </div>
    <!-- /#wrapper -->

    

</body>
<?php }?>
</html>

