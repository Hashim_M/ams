<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 

			
require'config.php';
  
	
	 
	if(isset($_GET['edit'])){
		
		$edit_id = $_GET['edit'];
		
		
		 $edit_query="SELECT * FROM item_categories WHERE cat_id='$edit_id ' LIMIT 1";
	 
	 $run_edit = $conn->query($edit_query);
	 
	 while($edit_row=mysqli_fetch_array($run_edit)){ 
		
		 
		
	
	  
  ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  <script type="text/javascript" src="jss/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
	$("#parent_cat").change(function() {
		$(this).after('<div id="loader"><img src="images/loading.gif" alt="loading subcategory" /></div>');
		$.get('loadsubcat.php?group=' + $(this).val(), function(data) {
			$("#sub_cat").html(data);
			$('#loader').slideUp(200, function() {
				$(this).remove();
			});
		});	
    });

});
</script>
  
   <script type="text/javascript">
  	function formValidator(){
	// Make quick references to our fields
	var cat = document.getElementById('asset-cat');
	var group = document.getElementById('group');
	var description = document.getElementById('cat-desc');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(cat, "Please enter only letters for your category name")){
				if(madeSelection(group, "Please Choose a group")){
					if(lengthRestriction(description, 30, 150)){
						
							return true;
						
					}
				}
				}
	
	
	return false;
	
}
	
function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}
  
  </script>

</head>

<body>

    <div id="wrapper">
       
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">EDIT ASSET CATEGORY</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
               
                          
			                     <form role="form" name="form" method="post"  onsubmit="return formValidator()" action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>?edit_form=<?php echo $edit_row['cat_id'];?>">
									 <label for="group">ASSET GROUP:</label>
									 <select class="form-control" name="group">
									 <option selected value='.\select\.'>..\select\..</option>
									<?php 
										       $sel_group = "SELECT * FROM item_group";
											   $run_query = $conn->query($sel_group);
											    
												while($row_group = mysqli_fetch_array($run_query)){
								
													 echo"<option  value='";
													 echo $row_group ['item_group_name']; 
													 echo " '>";
													echo $row_group ['item_group_name']; 
													echo "</option>";
										} ?>
									 </select>
									 </br>
									 
								   <label for ="category_name">CATEGORY NAME</label>
								   <input type="text" name="asset_cat" size="40" class="form-control" id="asset-cat" value="<?php echo $edit_row['category']; ?>" required/>
								</br>
								   <label for ="category_description">CATEGORY DESCRIPTION</label>
								   <textarea  colspan="5" rowspan="2" name="cat_description" id="cat-desc" class="form-control"><?php echo $edit_row['item_cat_description']; ?></textarea>
									
									  
							<button type="submit" name="login-btn" onClick="confirm('Are you sure, you want to save the update')" class="btn btn-success">Update Category</button>
							<button type="reset" class="btn btn-primary">Reset</button>
							</form>
							<?php }}?>

								<?php
									
									 if(isset($_POST['login-btn']))
									  {
									  global $row_group_id;
									  $edit_id = mysqli_real_escape_string($conn,$_GET['edit_form']);
										$cat = mysqli_real_escape_string($conn,$_POST['asset_cat']);
										$cat_desc = mysqli_real_escape_string($conn,$_POST['cat_description']);
										$grop = mysqli_real_escape_string($conn,$_POST['group']);
									
									
									
									
									
									$select_group = "SELECT item_group_id FROM item_group WHERE item_group_name = '%$grop%' ";
									$run_query = $conn->query($select_group);
									while($row = mysqli_fetch_array($run_query)){
										 
											$row_group_id = $row['item_group_id'];
										}
										
										
										
										$update_query =mysqli_query($conn,"UPDATE `item_categories` SET `item_group_id`='$row_group_id ',`item_cat_description`='$cat_desc',`date_of_modification`=now()
										 WHERE cat_id ='$edit_id'");
										if($update_query === TRUE)
										{
										echo "<script>alert('The Category is Successfully updated.')</script>";
										echo "<script>window.open('category.php','_self')</script>";
										}
										else
										{
							             echo $conn->error;
										}
									    }
									
	?>
	                      
    </div>
    <!-- /#wrapper -->


</body>
<?php }?>
</html>
