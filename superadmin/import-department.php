<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  <script>
$(document).ready(function () {
$('input[type=file]').change(function () {
var val = $(this).val().toLowerCase();
var regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|bmp|ppt|xls|csv|gif|png|jpg)$");
 if(!(regex.test(val))) {
$(this).val('');
alert('Please select correct file format');
} }); });
</script>

</head>

<body>

    <div id="wrapper">
     
                <div class="row">
                    <div class="col-lg-12">
                      <h2 class="page-header">IMPORT DEPARTMENT RECORDS</h2>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
              
                          
						<form role="form" name="form" method="post" action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>" enctype="multipart/form-data">
  
 
							  <label for ="asset value">Browse File name</label>
							  <input type="file" name="filename" value="" class="form-control" required/>
							  </br>
							  <button type="submit" name="import" class="btn btn-success">Import</button>
							  <a href="Department.php"><button type="submit"  class="btn btn-danger">Back</button></a>
							   
						</form>


                       

    </div>
    <!-- /#wrapper -->

    
</body>

</html>
<?php
if(isset($_POST["import"]))
{
require"config.php";

	$file = $_FILES['filename']['tmp_name'];
	$handle = fopen($file, "r");
	$c = 0;
	while(($col = fgetcsv($handle, 1000, ",")) !== false)
	{
		
 $col1 = $col[0];
 $col2 = $col[1];
 
                                $sql = "INSERT INTO affliate_department(affln_depart_id,affln_depart_name,affln_depart_code,date_modified) 
                                         VALUES('','".$col1."','".$col2."',now())";
                                       $result = $conn->query($sql);
										 $c = $c + 1;
										 }
											if ( $result=== TRUE) {
												       echo "<script>alert('File data successfully imported to database!!')</script>";
                                                       echo "<script>window.open('Department.php','_self')</script>";
											} else {
												echo "Error: " . $sql . "<br>" . $conn->error;
											}
                                            
											
													}
													
}?>