<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require'config.php';
     if(isset($_POST['submit'])){
	    global $id;
	    $fname =  mysqli_real_escape_string($conn,$_POST['fname']);
	    $lname=  mysqli_real_escape_string($conn,$_POST['lname']);
	    $hphone= mysqli_real_escape_string($conn,$_POST['hphone']);
		$cphone = mysqli_real_escape_string($conn,$_POST['cphone']);
		$gender =  mysqli_real_escape_string($conn,$_POST['gender']);
		$username = mysqli_real_escape_string($conn,$_POST['username']);
		$pass = mysqli_real_escape_string($conn,$_POST['pass']);
		$dat = mysqli_real_escape_string($conn,$_POST['date']);
		$permission =mysqli_real_escape_string($conn,$_POST['permission']);
	    
		 $insert_query = mysqli_query($conn,"INSERT INTO users (first_name,last_name,username,password,date_modified,dob,home_phone,cell_phone,gender,permission)
		 VALUE(' $fname',' $lname','$username','$pass',now(),'$dat','$hphone','$cphone','$gender ','$permission')") or die(mysqli_error($conn));
		 $sel_id = mysqli_query($conn, "SELECT user_id FROM users ORDER BY user_id DESC LIMIT 1") or die(mysqli_error($conn));
		 while($row = mysqli_fetch_assoc($sel_id)){
		     $id = $row['user_id'];
		 }
		 $insert_login =mysqli_query($conn,"INSERT INTO login_user(user_id,username,password,permission) 
		                  VALUES('$id','$username','$pass','$permission')")  or die(mysqli_error($conn));
		
		echo "<script>alert('User  is Successfully added.')</script>";
		echo "<script>window.open('profile.php','_self')</script>";
		
		 
		
		
		}
	    
	 
  
  
  
?>
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  <script type="text/javascript">
 

function formValidator(){
	// Make quick references to our fields
	var name = document.getElementById('asset-name');
	var s-code = document.getElementById('s-code');
	var code = document.getElementById('code');
	var state = document.getElementById('state');
	var description = document.getElementById('description');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(name, "Please enter only letters for your name")){
		if(isAlphanumeric(s-code, "Numbers and Letters Only")){
			if(isNumeric(code, "Please enter a valid tracking code")){
				if(madeSelection(state, "Please Choose approriate selection")){
					if(lengthRestriction(description, 10, 30)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>


</head>

<body>

    <div id="wrapper">
     
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">Register Users</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            
                          
			                    <form action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>" method= "post" enctype="multipart/form-data" onsubmit= 'return formValidator()'>
									 
									 
								       <label for ="permission">Permission:</label>
									   <input type="text" name="permission" size="40" class="form-control" id="code" required/>
										</br>
									   <label for ="fname">Firstname</label>
									   <input type="text" name="fname" size="40" class="form-control" id="" required/>
										</br>
										 <label for ="lname">Lastname</label>
									   <input type="text" name="lname" size="40" class="form-control" id="asset-name" required/>
										</br>
									   <label for ="uname">Email</label>
									   <input type="text" name="username" size="40" class="form-control" id="email"required/>
										</br>
										 <label for ="passwd">Password</label>
									   <input type="text" name="pass" id="s-code" size="40" class="form-control" required/>
										</br>
									   <label for ="dob">Date of Birth</label>
									   <input type="date" name="date" size="40" class="form-control datepicker" id="s-code" required/>
										</br>
									   
									   <label for ="hphone">Home Phone</label>
									  <input type="text" name="hphone" size="40" class="form-control" id="code" required/>
										</br>
									 <label for ="hphone">Cell Phone</label>
									  <input type="text" name="cphone" size="40" class="form-control" id="code" required/>
										</br>
									   <label for ="status">Gender</label>
									   <input type="radio" name="gender" size="40"  value="Male" required/>Male &nbsp;
									   <input type="radio" name="gender" size="40"  value="Female" required/> Female
										</BR>
										
									  <button type="submit" name="submit" class="btn btn-success">ADD ASSET</button></h1>
									  <button type="reset" name="submit"  class="btn btn-danger">RESET</button></h1>
									   
									 </form>
								

                            
                        

    </div>
    <!-- /#wrapper -->

    
</body>
<?php }?>
</html>
