<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{
	require'config.php';

if(isset($_POST['submit'])){
	
	 global $item_id;
	 global $qty1;
	 global $qty;
	 global $item_assigned;
	 global  $return_date;
	 global $assigne;
	 
	$item_assigned = mysqli_real_escape_string($conn,$_POST['item']);
	$user = $_SESSION['user'];
    $return_date1 = mysqli_real_escape_string($conn,$_POST['item']);
	$return_date2 = mysqli_real_escape_string($conn,$_POST['item1']);
	$return_date3 = mysqli_real_escape_string($conn,$_POST['item2']);
	$return_date =$return_date3 + $return_date2 + $return_date1;
	$assigne = mysqli_real_escape_string($conn,$_POST['assignee']);
	
	
	$sel_invent = "SELECT item FROM inventory WHERE item  = '$item_assigned' LIMIT 1";
	$result = $conn->query($sel_invent) or die(mysqli_error($conn));
	    
		    while($row = mysqli_fetch_array( $result )){
			
			    $qty = $row['qty']; 
				$item_id = $row['item_id'];
				 
			}
		$qty1=$qty-1;
		$update_invent = "UPDATE inventory SET qty='$qty1' WHERE item_id='$item_id'";
		$res = $conn->query($update_invent);
		
		$query_assignment = mysqli_query($conn, "SELECT item_id FROM item_allocations WHERE item_id = '$item_id'") or die(mysqli_error($conn));
		if(mysqli_num_rows($query_assignment) === null)
           {
	
	$query = "INSERT INTO item_allocations (item_id,user,assignee,date_of_allocation, date_of_return)VALUES('$item_id','$user','$assigne',now(), '$return_date')";
	
	$run_query = $conn->query($query) or die(mysqli_error($conn));
	

		
		echo "<script>alert('The Allocation is Successfully.')</script>";
		echo "<script>window.open('view-assignment.php','_self')</script>";
		
	    }
	else
	{
	  echo "<script>alert('The item is already allocated.')</script>";
	  echo "<script>window.open('view-assignment.php','_self')</script>";
	}
	
	
	
	}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   
   
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  <!-- date picker libraries -->
  <link rel="stylesheet" href="stylesheet/jquery-ui.css">
  <script src="jss/jquery-1.10.2.js"></script>
  <script src="jss/jquery-ui.js"></script>
   <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
  <script type="text/javascript">
 

function formValidator(){
	// Make quick references to our fields
	var name = document.getElementById('firstname');
	var zip = document.getElementById('zip');
	var code = document.getElementById('t-code');
	var state = document.getElementById('state');
	var description = document.getElementById('description');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(name, "Please enter only letters for your name")){
		if(isAlphanumeric(zip, "Numbers and Letters Only")){
			if(isNumeric(code, "Please enter a valid tracking code")){
				if(madeSelection(state, "Please Choose approriate selection")){
					if(lengthRestriction(description, 6, 8)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z\s]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>

</head>

<body>

    <div id="wrapper">
       
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">ADD ALLOCATION</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
               
								 <form action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>" method= "post" onsubmit='return formValidator()'>
							 
							   
							   <label for ="group_name">ASSIGNEE</label>
							   <input type="text" name="assignee" id="firstname" class="form-control" value="" size="40" required/>
								
							   <label for ="group_description">ITEM ALLOCATED</label>
							 <select name="item" class="form-control" >
							   <option selected value='.\select\.'>..\select\..</option>
									<?php 
										       $sel_item = "SELECT * FROM items ";
											   $result = $conn->query($sel_item);
											    
												while($row_group = mysqli_fetch_array($result)){
												
										            
													 
													 echo"<option  value='";
													 echo $row_group ['item']; 
													 echo " '>";
													echo $row_group ['item']; 
													echo "</option>";
										} ?>
							 </select>
							 </br>
								
								
								 
								  <label for ="group_name">Return Date</label>
								  	 <select name="item" >
									 <option value="day"> Day </value>
									 <?php 
									 $count = 1;
									 for( $count=1; $count<=31; ++$count){
									 
									     echo"<option  value='";
													 echo $count; 
													 echo " '>";
													echo $count; 
													echo "</option>";
									 
									 }
									 
									 ?>
									 </select>
									 &nbsp;&nbsp;&nbsp;&nbsp;
									  <select name="item1"  >
									  <option value="months">Months</option>
									 <?php 
									 $count = 1;
									 for( $count=1; $count<=12; ++$count){
									 
									     echo"<option  value='";
													 echo $count; 
													 echo " '>";
													echo $count; 
													echo "</option>";
									 
									 }
									 
									 ?>
									 </select>
									 &nbsp;&nbsp;&nbsp;&nbsp;
									  <select name="item2"  >
									  <option value="Year">Year</option>
									 <?php 
									 $count = 2015;
									 for( $count=2015; $count<=2030; ++$count){
									 
									     echo"<option  value='";
													 echo $count; 
													 echo " '>";
													echo $count; 
													echo "</option>";
									 
									 }
									 
									 ?>
									 </select>
							 	</br>
							   
							  <button type="submit" name="submit" onClick="confirm('Do you want allocate this item?')" class="btn btn-success">ALLOCATE</button></h1>
							  <button type="reset" name="submit"  class="btn btn-danger">RESET</button></h1> 
							 			</form>
								

                    
            
        </div>
        <!-- /#page-wrapper -->


</body>
<?php }?>
</html>
