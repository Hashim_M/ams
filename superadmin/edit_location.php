<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 
	require'config.php';
  
		if(isset($_GET['edit'])){
		global $affln_id;
		global $faculty;
		$edit_id = $_GET['edit'];
			
			$edit_query="SELECT * FROM location WHERE loc_id='$edit_id ' LIMIT 1";
	 
	 $run_edit = $conn->query($edit_query);
	 
	 while($edit_row=mysqli_fetch_array($run_edit)){ 
		
		
	
	
			?>
			
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  
  <script type="text/javascript">
  

function formValidator(){
	// Make quick references to our fields
	var firstname = document.getElementById('firstname');
	var addr = document.getElementById('addr');
	var zip = document.getElementById('zip');
	var state = document.getElementById('state');
	var username = document.getElementById('username');
	var email = document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(firstname, "Please enter only letters for your name")){
		if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
			if(isNumeric(zip, "Please enter a valid zip code")){
				if(madeSelection(state, "Please Choose a contract type")){
					if(lengthRestriction(username, 15, 30)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
					}
				}
			}
		}
	}
	
	
	return false;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}
</script>
</script><script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
	$("#parent_cat").change(function() {
		$(this).after('<div id="loader"><img src="images/loading.gif" alt="loading category" /></div>');
		$.get('faculty-building.php?parent_cat=' + $(this).val(), function(data) {
			$("#sub_cat").html(data);
			$('#loader').slideUp(200, function() {
				$(this).remove();
			});
		});	
    });

});
</script>

</head>

<body>

    <div id="wrapper">
        
                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">EDIT LOCATION</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
           
		   
                          
			                        
							 <form role="form" name="form" method="GET" onsubmit="return formValidator()" action="edit_location.php?edit_form=<?php echo $edit_row['loc_id'];?>">
								
								
								
								<label for="email">Building:</label><br />
								<select name="sub_cat" class="form-control" id="sub_cat">
								<select name="sub_cat" class="form-control" id="sub_cat">
									<?php 
										       $sel_group ="SELECT * FROM  building";
											   $result=$conn->query( $sel_group);
											    
												while($row_group =mysqli_fetch_array($result)){
												
										            
													 
													 echo"<option  value='";
													 echo $row_group ['building_name']; 
													 echo " '>";
													echo $row_group ['building_name']; 
													echo "</option>";
										} ?>
									</select>

								</select>
							

								<label for="email">Location Name:</label><br />
								<input name="name"  type="text" id="firstname" value="<?php echo $edit_row['loc_name']; ?>" class="form-control" required/><br />

								<label for="email">Location Acronym:</label><br />
								<input name="acronym" id="firstname" value="<?php echo $edit_row['loc_acronym']; ?>" type="text" class="form-control" required/><br />

								<label for="email">Location Code:</label><br />
								<input name="code"  id="zip" type="text" value="<?php echo $edit_row['loc_code']; ?>" class="form-control" required/><br />

									<label for ="email">Location Status:</label><br />
								<input name="status" type="radio"  value="Occupied" > Occupied &nbsp;
								<input name="status" type="radio"  value="Not Occupied" > Not Occupied &nbsp;
								<input name="status" type="radio"  value="BOOKED" > BOOKED 
								</br>
								<label for="email">Description:</label><br />
								<textarea col="2" rows="2" name="description" id="username" class="form-control" ><?php echo $edit_row['description']; ?></textarea>
								</br>
								<button type="submit" name="login-btn" onClick ="confirm('Are you sure the location doesn't exist?')"class="btn btn-success">Update Location</button>
								<button type="reset" class="btn btn-primary">Reset</button>
								</form>
								<?php }}?>

														<?php


									 if(isset($_GET['login-btn']))
									  {
									
									  global $building_id;
									$edit_id =$_GET['edit_form'];
									$loc_name = mysqli_real_escape_string($conn,$_GET['name']);
									$loc_code =mysqli_real_escape_string($conn,$_GET['code']);
									$loc_acronym =mysqli_real_escape_string($conn,$_GET['acronym']);
									$loc_descrip =mysqli_real_escape_string($conn,$_GET['description']);
									
									$building = mysqli_real_escape_string($conn,$_GET['sub_cat']);
									 $status = mysqli_real_escape_string($conn,$_GET['status']);
									
									
										$sel_query1 = "SELECT building_id FROM building WHERE building_name ='$building' LIMIT 1 ";
										$sel_query_result= $conn->query($sel_query1 );
											while ($row_data = mysqli_fetch_array($sel_query_result)){
												
												$building_id = $row_data['building_id'];
												}
 		
							
									
									$update_query ="UPDATE `location` SET `loc_code`='$loc_code',`loc_acronym`='$loc_acronym',`loc_name`='$loc_name',description='$loc_descrip',
										`date_modified`=now(),status='$status', building_id = '$building_id' WHERE loc_id ='$edit_id'";
										   $sel_query_result1=$conn->query($update_query);
										   if( $sel_query_result1 === true )
										{
										echo "<script>alert('The Location is Successfully Updated.')</script>";
										echo "<script>window.open('locations.php','_self')</script>";
										}
										else
										{
											echo"update has failed";
										}
									
									 
									
											
											}?>

                         

    </div>
    <!-- /#wrapper -->

   

</body>
<?php }?>
</html>
