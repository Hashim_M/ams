<?php
session_start();
if(!isset($_SESSION['user'])){

   header('location:../index.php');	
	
	}else{ 

			
require'config.php';
  
	
	 
	if(isset($_GET['edit'])){
		
		$edit_id = $_GET['edit'];
		
		
		 $edit_query="SELECT * FROM item_transfer WHERE item_transfer_id='$edit_id ' LIMIT 1";
	 
	 $run_edit = $conn->query($edit_query);
	 
	 while($edit_row=mysqli_fetch_array($run_edit)){ 
		
		 
		
	
	  
  ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ASSET MANAGER</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
	
<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>

<script src="jss/jquery.js" type="text/javascript"></script>
<script src="jss/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
  <script type="text/javascript">
  	function formValidator(){
	// Make quick references to our fields
	var cat = document.getElementById('asset-cat');
	var group = document.getElementById('group');
	var description = document.getElementById('cat-desc');
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(cat, "Please enter only letters for your category name")){
				if(madeSelection(group, "Please Choose a group")){
					if(lengthRestriction(description, 30, 150)){
						
							return true;
						
					}
				}
				}
	
	
	return false;
	
}
	
function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "Please Choose"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}
  
  </script>
 

</head>

<body>

    <div id="wrapper">

                <div class="row">
                    <div class="col-lg-12">
                      <h1 class="page-header">ASSET TRANSFER</h1>
                    </div>
                   <!-- /.col-lg-12 -->
                </div>
                
                          
			                 <form role="form" name="form" method="post" action="<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]);?>?edit_form=<?php echo $edit_row['item_transfer_id'];?>" onsubmit='return formValidator()'>
     
								
								    <label for ="category_name">ITEM</label>
								   <input type="text" name="item" id="asset-cat" size="40" value="<?php echo $edit_row['item_transferred']; ?>" class="form-control" required/>
									
									<label for ="category_name">INITIAL LOCATION</label>
								   <select class="form-control" name="initial-location" id="state">
									     <option selected value='<?php echo $edit_row['initial_location']; ?>'><?php echo $edit_row['initial_location']; ?></option>
										  <?php 
										       $sel_group ="SELECT * FROM location";
											    $group_result = $conn->query($sel_group);
												while($row_group = mysqli_fetch_array($group_result)){
													 echo"<option  value='";
													 echo $row_group ['loc_name']; 
													 echo " '>";
													echo $row_group ['loc_name']; 
													echo "</option>";
										} ?>
										 </select> 
									
									<label for ="category_name">NEW LOCATION</label>
								 <select class="form-control" name="new-location" id="state">
									     <option selected value='<?php echo $edit_row['new_location']; ?>'><?php echo $edit_row['new_location']; ?></option>
										  <?php 
										       $sel_group ="SELECT * FROM location";
											    $group_result = $conn->query($sel_group);
												while($row_group = mysqli_fetch_array($group_result)){
													 echo"<option  value='";
													 echo $row_group ['loc_name']; 
													 echo " '>";
													echo $row_group ['loc_name']; 
													echo "</option>";
										} ?>
										 </select> 
									
								   
								   <label for ="category_description">REASON</label>
								   <textarea  colspan="5" rowspan="2" name="reason" class="form-control" id="cat-desc"><?php echo $edit_row['reason_for_transfer']; ?></textarea>
									</br>
									
								  <button type="submit" name="submit" class="btn btn-success">ADD GROUP</button>
								  <a href="asset-transfer.php"><button type="submit"   class="btn btn-primary">BACK</button></a>
								 </form>
<?php }}?>

                              <?php 
							  
						require'config.php';
						if(isset($_POST['submit'])){
							global $row_group_id;
							$id =mysqli_real_escape_string($conn,$_GET['edit_form']);
							$item = mysqli_real_escape_string($conn,$_POST['item']);
							$new_loc = mysqli_real_escape_string($conn,$_POST['new-location']);
							$initial_loc = mysqli_real_escape_string($conn,$_POST['initial-location']);
							$reazon = mysqli_real_escape_string($conn,$_POST['reason']);
							$transferred_by = mysqli_real_escape_string($conn,$_SESSION['user']);
							

							$query = mysqli_query($conn,"UPDATE item_transfer SET item_transferred='$item',initial_location='$initial_loc',
							        new_location='$new_loc', reason_for_transfer='$reazon', date_of_transfer = now(), transferred_by ='$transferred_by' 
									WHERE item_transfer_id = '$id'") or die(mysqli_error($conn));
								
								if($query === TRUE){
									   echo '<script>alert("You have successful updated the record ")</script>';
										echo '<script>window.open("asset-transfer.php","_self")</script>';
										 }
										else{
									
										 echo "UPDATING THE ASSET TRANSFER RECORDS, IS FATALLY UNSUCCESSFUL";
									
										   }}
								
		


?>
							  
							  
							  
							  
							  
							  
							  
							  
							

                       

    </div>
    <!-- /#wrapper -->

  

</body>
<?php }?>
</html>
