<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Asset Manager</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all"> 
	<link rel="stylesheet" href="css/responsiveslides.css" />  
	<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="js/cufon-yui.js" type="text/javascript"></script>
	<script src="js/cufon-replace.js" type="text/javascript"></script>
	<script src="js/Vegur_500.font.js" type="text/javascript"></script>
	<script src="js/Ropa_Sans_400.font.js" type="text/javascript"></script> 
	<script src="js/FF-cash.js" type="text/javascript"></script>	  
	<script src="js/tms-0.3.js" type="text/javascript"></script>
	<script src="js/tms_presets.js" type="text/javascript"></script>
	<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
	<script src="js/responsiveslides.js"></script>
	<link href="stylesheet/facebox.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="jss/argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>
    <script src="jss/jquery.js" type="text/javascript"></script>
	<script src="jss/facebox.js" type="text/javascript"></script>
	  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'loading.gif',
        closeImage   : 'closelabel.png'
      })
    });
  </script>
	<script>
		$(function () {
		  $("#slider").responsiveSlides({
			auto: true,
			pager: true,
			nav: true,
			speed: 500,
			maxwidth: 960,
			namespace: "centered-btns"
		  });
		});
	</script>
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
	</div>
	<![endif]-->
	<!--[if lt IE 9]>
 		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->
</head>
<body id="page1">
	<!--==============================header=================================-->
	<header>
		<div class="border-bot">
			<div class="main zerogrid">
				<!--<h1><a href="index.html">InternetCafe</a></h1>-->
				<nav>
				
									<strong class="title-1"><img src="images/icipe.jpeg" height="80" width="100" alt="" />ICIPE THOMAS ODHIAMBO CAMPUS ASSET <br> MANAGEMENT SYSTEM (ITOCAMS)</strong>
								</div>
					<ul class="menu">
					    
						<li><a  rel="facebox"  class="active" href="login.php">Login</a></li>
						
					</ul>
				</nav>
				<div class="clear"></div>
			</div>
		</div>
		<div class="row-bot">
			<div class="slider zerogrid">
		   		<div class="rslides_container">
					<ul class="rslides" id="slider">
						<li><img src="images/img1.jpg" alt="" /></li>
						<li><img src="images/img2.jpg" alt="" /></li>
						<li><img src="images/img3.jpg" alt="" /></li>
					</ul>
				</div>
		    </div>
	    </div>
	</header>
	<!--==============================content================================-->
	<section id="content">
		<div class="main">
			<div class="zerogrid">
				<div class="row">
					<article class="col-2-3"><div class="wrap-col">
						<h2>Welcome!</h2>
						<p class="p2">ICIPE THOMAS ODHIAMBO CAMPUS ASSET MANAGEMENT SYSTEM (ITOCAMS):</p>
						<p class="p3">ITOCAMS is intended to help users to keep track of information related to the institution assets
						and help to manage the assets.</p>
						
						<!--<h3 class="p1">Services Offered By EUARS:</h3>
						<div class="row">
							<div class="col-1-2"><div style="padding-right: 10px;">
								<ul class="list-1">
									<li><a href="#">Provide facility to create account for users</a></li>
									<li><a href="#">Provide facility to enter details of the assets</a></li>
									<li><a href="#">Provide facility to delete assets </a></li>
									<li><a href="#">Provide facility to modify the assets</a></li>
									
								</ul>
							</div></div>
							<div class="col-1-2">
								<ul class="list-1 indent-bot">
									<li>Provide facility to generate reports</li>
									<li>Provide facility for creating requisition</li>
									<li>Provide facility for enter contractors detail</li>
									<li>Provide facility for assigning assets to users</li>
								</ul>
								
							</div>
						</div>-->
					</div>		
			  </article>
					<article class="col-1-3"><div class="wrap-col">
						<div class="indent-top indent-left">
							<!--
							<div class="wrapper margin-bot">
								 <figure class="img-indent-r"><a href="#"><img src="images/page1-img1.png" alt=""></a></figure>
								<div class="extra-wrap">
									<strong class="title-1">Tell Your <strong>Friends</strong><em>About</em><em>Our ITOCAMS</em></strong>
								</div>
								
							</div>
							-->
							
						</div>
					</div></article>
				</div>
			</div>
		</div>
	</section>
	<!--==============================footer=================================-->
	<footer>
		<div class="main">
			<div class="zerogrid">
				<div class="wrapper">
					<div class="col-1-4">
						
					</div>
					<div class="col-2-4">
						<div class="wrap-col indent-top2">
							<p class="prev-indent-bot">
							Phone: +254 706 805 838 Email: <a href="#">info@icipe.org</a> <br>
							&copy; 2016. <br>
							</p>
						</div>
					</div>
					<div class="col-1-4">
						<ul class="wrap-col list-services">
							<li><a class="item-1" href="#"></a></li>
							<li><a class="item-2" href="#"></a></li>
							<li><a class="item-3" href="#"></a></li>
							<li><a class="item-4" href="#"></a></li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script type="text/javascript"> Cufon.now(); </script>
	<script type="text/javascript">
		$(window).load(function() {
			$('.slider')._TMS({
				duration:1000,
				easing:'easeOutQuint',
				preset:'diagonalFade',
				slideshow:7000,
				banners:false,
				pauseOnHover:true,
				pagination:true,
				pagNums:false
			});
		});
	</script>
</body>
</html>