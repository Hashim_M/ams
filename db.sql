-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2016 at 06:13 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `affliate_department`
--

CREATE TABLE IF NOT EXISTS `affliate_department` (
  `affln_depart_id` int(10) NOT NULL AUTO_INCREMENT,
  `affln_depart_name` varchar(55) DEFAULT NULL,
  `affln_id` int(12) DEFAULT NULL,
  `affln_depart_code` varchar(10) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`affln_depart_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `affliate_department`
--

INSERT INTO `affliate_department` (`affln_depart_id`, `affln_depart_name`, `affln_id`, `affln_depart_code`, `date_modified`) VALUES
(2, 'COMPSCI', 0, 'EUF004D001', '2016-03-21 09:42:51'),
(3, 'ECONSTAT', 5, 'EUF005D001', '2016-03-21 09:43:20'),
(4, 'ANHE', 3, 'EUF002D001', '2016-03-21 09:44:53'),
(6, 'GENDER', 7, 'EUF007D001', '2016-03-21 09:46:46'),
(7, 'TRANSPORT', 8, 'EUF003D001', '2016-03-21 09:51:57'),
(8, 'CATERING ', 8, 'EUF007D002', '2016-03-21 09:52:48'),
(9, 'GUIDANCEANDCOUNSELLING', 8, 'EUF007D003', '2016-03-21 09:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `affliate_faculty`
--

CREATE TABLE IF NOT EXISTS `affliate_faculty` (
  `affln_id` int(12) NOT NULL AUTO_INCREMENT,
  `affln_name` varchar(50) DEFAULT NULL,
  `affln_code` varchar(12) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`affln_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=9 ;

--
-- Dumping data for table `affliate_faculty`
--

INSERT INTO `affliate_faculty` (`affln_id`, `affln_name`, `affln_code`, `date_modified`) VALUES
(1, 'FEDCOS', 'EUF001', '2016-03-21 09:30:21'),
(3, 'AGRICULTURE', 'EUF002', '2016-03-21 09:30:43'),
(4, 'SCIENCE', 'EUF003', '2016-03-21 09:31:07'),
(5, 'FASS', 'EUF004', '2016-03-21 09:31:28'),
(6, 'FERD', 'EUF005', '2016-03-21 09:31:52'),
(7, 'GENDER', 'EUF006', '2016-03-21 09:32:20'),
(8, 'OTHER', 'EUF007', '2016-03-21 09:51:21');

-- --------------------------------------------------------

--
-- Table structure for table `affliation`
--

CREATE TABLE IF NOT EXISTS `affliation` (
  `affliate_id` int(12) NOT NULL AUTO_INCREMENT,
  `affliate_name` varchar(55) DEFAULT NULL,
  `affliate_code` varchar(25) DEFAULT NULL,
  `affliate_description` varchar(255) DEFAULT NULL,
  `founding_date` date DEFAULT NULL,
  PRIMARY KEY (`affliate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `affliation`
--

INSERT INTO `affliation` (`affliate_id`, `affliate_name`, `affliate_code`, `affliate_description`, `founding_date`) VALUES
(1, 'ADMINISTRATION', 'EU001 ', 'ADMINISTRATIVE  AND MANAGEMENT  ROLE OF THE EU ', '2016-03-21'),
(2, 'FACULTY', 'EU002', 'HIGHEST ACADEMIC LEVEL IN THE EU', '2016-03-21'),
(7, 'DEPARTMENT', 'EU003 ', 'ALL THE DEPARTMENTAL ENTITIES WITHIN THE UNIVERSITY ', '2016-04-03');

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE IF NOT EXISTS `building` (
  `building_id` int(12) NOT NULL AUTO_INCREMENT,
  `building_code` varchar(20) DEFAULT NULL,
  `building_name` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `date_of_modification` date DEFAULT NULL,
  PRIMARY KEY (`building_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`building_id`, `building_code`, `building_name`, `status`, `date_of_modification`) VALUES
(2, 'EUB002', 'SCIENCECOMPLEX', 'Good', '2016-04-03'),
(3, 'EUB003', 'FASSBLOCKA', 'Good', '2016-04-03'),
(4, 'EUB004', 'FASSBLOCKB', 'Good', '2016-04-03'),
(5, 'EUB005', 'TRANSPORTSTORE', 'Good', '2016-04-03'),
(6, 'EUB012', 'GenderBlock', 'Good', '2016-04-03');

-- --------------------------------------------------------

--
-- Table structure for table `contract_details`
--

CREATE TABLE IF NOT EXISTS `contract_details` (
  `contract_id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(55) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `telephone` int(14) DEFAULT NULL,
  `po_box` int(14) DEFAULT NULL,
  `postal_code` int(14) DEFAULT NULL,
  `location` varchar(25) DEFAULT NULL,
  `contract_type` varchar(25) DEFAULT NULL,
  `item` varchar(25) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`contract_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contract_details`
--

INSERT INTO `contract_details` (`contract_id`, `company`, `email`, `telephone`, `po_box`, `postal_code`, `location`, `contract_type`, `item`, `description`, `date_modified`) VALUES
(1, ' comptech', ' dancun.ogindo3@gmail.com', 2147483647, 63564, 52626, 'nairobi', ' ', ' computers', '  all the computers accessories for ict management of the university.', '2016-04-03 11:48:30');

-- --------------------------------------------------------

--
-- Table structure for table `depreciation`
--

CREATE TABLE IF NOT EXISTS `depreciation` (
  `depreciation_id` int(12) NOT NULL AUTO_INCREMENT,
  `depreciation_type` varchar(255) DEFAULT NULL,
  `depreciation_rate` double(12,0) DEFAULT NULL,
  `depreciation_formula` varchar(20) NOT NULL,
  `depreciation_description` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`depreciation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `depreciation`
--

INSERT INTO `depreciation` (`depreciation_id`, `depreciation_type`, `depreciation_rate`, `depreciation_formula`, `depreciation_description`, `date_modified`) VALUES
(1, 'straightline', 20, '0', 'for calculating solvent assets', '2016-03-05 09:42:35');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `item_id` int(12) NOT NULL AUTO_INCREMENT,
  `item` varchar(100) DEFAULT NULL,
  `qty` int(255) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`item_id`, `item`, `qty`, `status`, `modified_by`, `date_modified`) VALUES
(1, NULL, 2, 'GOOD', '0', '2016-03-21 09:58:30'),
(2, NULL, 1, 'GOOD', '0', '2016-03-21 09:59:50'),
(3, NULL, 1, 'GOOD', '0', '2016-03-21 10:08:05'),
(4, NULL, 1, 'GOOD', '0', '2016-03-21 10:09:51'),
(5, NULL, 1, 'GOOD', '0', '2016-03-21 15:43:15'),
(6, NULL, 2, 'GOOD', '0', '2016-03-21 15:48:35'),
(13, 'gongongeriflowerfarm', 1, 'GOOD', 'joy@gmail.com', '2016-04-03 18:37:25'),
(14, 'lorry', 12, 'GOOD', 'admin@gmail.com', '2016-03-29 19:12:05'),
(15, 'car', 12, 'GOOD', 'joy@gmail.com', '2016-04-03 19:18:28'),
(16, 'chairs', 2314, 'GOOD', 'joy@gmail.com', '2016-04-03 19:35:08'),
(17, 'OFFICETABLES', 12, 'GOOD', 'joy@gmail.com', '2016-04-04 10:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(15) NOT NULL AUTO_INCREMENT,
  `item` varchar(20) DEFAULT NULL,
  `group_id` int(15) DEFAULT NULL,
  `cat_id` int(15) DEFAULT NULL,
  `loc_id` int(12) DEFAULT NULL,
  `serial_number` varchar(32) DEFAULT NULL,
  `qty` int(30) DEFAULT '0',
  `warranty_duration` varchar(10) DEFAULT NULL,
  `item_description` varchar(250) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item`, `group_id`, `cat_id`, `loc_id`, `serial_number`, `qty`, `warranty_duration`, `item_description`, `status`, `date_modified`) VALUES
(13, 'gongongeriflowerfarm', 1, 7, 2, 'plot#12', 1, 'n/a', 'flower farm', 'GOOD', '2016-04-03 15:37:25'),
(14, 'bota', 1, 7, 2, 'plot#13', 1, 'n/a', 'flower farm', 'GOOD', '2016-04-03 16:10:13'),
(15, 'car', 0, 4, 2, 'KRG1235T', 12, '2years', 'maent for university management staff', 'GOOD', '2016-04-03 16:18:28'),
(16, 'chairs', 0, 5, 2, 'EUC001', 2314, '2years', 'student chairs', 'GOOD', '2016-04-03 16:35:08'),
(17, 'OFFICETABLES', 0, 0, 6, 'EUF002', 12, '2years', 'office tables for cod offices', 'GOOD', '2016-04-04 07:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `item_allocations`
--

CREATE TABLE IF NOT EXISTS `item_allocations` (
  `allocation_id` int(10) NOT NULL,
  `item_id` int(10) DEFAULT NULL,
  `user` varchar(10) DEFAULT NULL,
  `assignee` varchar(255) DEFAULT NULL,
  `date_of_allocation` date DEFAULT NULL,
  `date_of_return` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_allocations`
--

INSERT INTO `item_allocations` (`allocation_id`, `item_id`, `user`, `assignee`, `date_of_allocation`, `date_of_return`) VALUES
(0, 0, 'admin@gmai', 'joyce', '2016-03-31', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `item_categories`
--

CREATE TABLE IF NOT EXISTS `item_categories` (
  `cat_id` int(10) NOT NULL AUTO_INCREMENT,
  `item_group_id` int(10) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `item_cat_description` varchar(255) DEFAULT NULL,
  `date_of_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `item_group_id` (`item_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `item_categories`
--

INSERT INTO `item_categories` (`cat_id`, `item_group_id`, `category`, `item_cat_description`, `date_of_modification`) VALUES
(3, 3, 'COMBINEHARVESTER', 'ALL THE COMBINE HARVESTERS POSSESSED', '2016-03-21 08:45:58'),
(4, 2, 'salooncar', 'for university management use', '2016-04-03 12:22:39'),
(5, 1, 'tables', 'lecture hall tables', '2016-04-03 18:14:07'),
(7, 4, 'flowerfarms', 'designated for flower plantation', '2016-04-03 18:12:13');

-- --------------------------------------------------------

--
-- Table structure for table `item_group`
--

CREATE TABLE IF NOT EXISTS `item_group` (
  `item_group_id` int(10) NOT NULL AUTO_INCREMENT,
  `item_group_name` varchar(55) DEFAULT NULL,
  `item_group_desc` varchar(255) DEFAULT NULL,
  `date_of_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`item_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `item_group`
--

INSERT INTO `item_group` (`item_group_id`, `item_group_name`, `item_group_desc`, `date_of_modification`) VALUES
(1, 'FURNITURE ', 'ALL FURNITURES IN EGERTON UNIVERSITY', '2016-03-21 08:20:56'),
(2, 'VEHICLES ', 'ALL VEHICLES OWNED BY THE EU', '2016-04-03 18:13:31'),
(4, 'LAND', 'ALL THE LAND POSSESSION OWNED BY THE EU', '2016-03-31 09:34:34'),
(5, 'FARMMACHINERIES', 'ALL FARMS OPERATION MACHINERIES OWNED BY THE EU', '2016-03-31 09:35:26'),
(6, 'BUILDING ', 'ALL THE PREMISES AND CAMPUSES OWNED BY THE EU', '2016-04-03 13:00:57');

-- --------------------------------------------------------

--
-- Table structure for table `item_transfer`
--

CREATE TABLE IF NOT EXISTS `item_transfer` (
  `item_transfer_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_transferred` varchar(30) DEFAULT NULL,
  `initial_location` varchar(50) DEFAULT NULL,
  `new_location` varchar(50) DEFAULT NULL,
  `reason_for_transfer` varchar(255) DEFAULT NULL,
  `date_of_transfer` date DEFAULT NULL,
  `transferred_by` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`item_transfer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `item_transfer`
--

INSERT INTO `item_transfer` (`item_transfer_id`, `item_transferred`, `initial_location`, `new_location`, `reason_for_transfer`, `date_of_transfer`, `transferred_by`) VALUES
(2, 'Lorry', 'EDUCATIONTHEATRE ', 'EDUCATIONTHEATRE ', 'FOR FERRYING BUILDING MATERIALS', '2016-04-03', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `loc_id` int(12) NOT NULL AUTO_INCREMENT,
  `loc_code` varchar(12) DEFAULT NULL,
  `loc_acronym` varchar(20) DEFAULT NULL,
  `loc_name` varchar(50) DEFAULT NULL,
  `affln_id` int(12) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `building_id` int(12) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `date_modified` date DEFAULT NULL,
  PRIMARY KEY (`loc_id`),
  KEY `affln_id` (`affln_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`loc_id`, `loc_code`, `loc_acronym`, `loc_name`, `affln_id`, `description`, `building_id`, `status`, `date_modified`) VALUES
(2, 'COMPLX001001', 'ET1', 'EDUCATIONTHEATRE', 1, 'LECTURE HALL AND CONFERENCE ROOM FOR THE UNIVERSITY', NULL, 'Not Occupied', NULL),
(3, 'EUCOMPLX0010', 'ET2', 'EDUCATIONTHEATRE', 1, 'LECTURE HALL AND CONFERENCE CENTER', NULL, 'Occupied', NULL),
(6, 'EUB01201', 'G1', 'CODoffice', NULL, 'THE COD OFFICE ', 6, 'Occupied', '2016-04-03'),
(7, 'EUB00202', 'COMPLAB', 'COMPUTERLAB', NULL, 'COMPUTER SCIENCE LABS FOR PRACTICAL SESSION', 2, 'Occupied', '2016-04-03');

-- --------------------------------------------------------

--
-- Table structure for table `login_user`
--

CREATE TABLE IF NOT EXISTS `login_user` (
  `user_id` int(13) NOT NULL DEFAULT '0',
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `login_attempt` int(10) DEFAULT NULL,
  `permission` int(10) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_user`
--

INSERT INTO `login_user` (`user_id`, `username`, `password`, `login_attempt`, `permission`) VALUES
(0, 'dan@gmail.com', 'dan', 0, 1),
(1, 'admin@gmail.com', 'admin', 0, 2),
(8, 'joy@gmail.com', 'joy', NULL, 1),
(9, 'superadmin@gmail.com', 'super', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `remote_addr` varchar(128) DEFAULT NULL,
  `request_uri` varchar(255) DEFAULT NULL,
  `referrer` varchar(12) DEFAULT NULL,
  `useragent` varchar(10) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `request_id` int(10) NOT NULL AUTO_INCREMENT,
  `requester` varchar(65) DEFAULT NULL,
  `req_type` varchar(8) DEFAULT NULL,
  `item` varchar(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_submitted` datetime DEFAULT NULL,
  `approved_by` int(10) DEFAULT NULL,
  `date_approved` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `status1` varchar(20) DEFAULT NULL,
  `status2` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `item_id` (`item`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`request_id`, `requester`, `req_type`, `item`, `description`, `date_submitted`, `approved_by`, `date_approved`, `status`, `status1`, `status2`) VALUES
(2, 'admin@gmail.com', 'General ', 'lorry', 'for ferrying building materials in the site', '2016-04-03 11:44:13', 0, '2016-04-03 11:44:55', 'accepted', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `request_type`
--

CREATE TABLE IF NOT EXISTS `request_type` (
  `request_type_id` int(12) NOT NULL AUTO_INCREMENT,
  `request_type` varchar(30) DEFAULT NULL,
  `request_type_description` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`request_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `request_type`
--

INSERT INTO `request_type` (`request_type_id`, `request_type`, `request_type_description`, `date_modified`) VALUES
(1, 'General', 'for all staff or done by a group of staff', '2016-04-03 13:13:26'),
(2, 'SPECIFIC', 'MEANT FOR SPECIFIC PERSON', '2016-04-03 13:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `home_phone` int(10) DEFAULT NULL,
  `cell_phone` int(10) DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `permission` int(10) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `password`, `date_modified`, `dob`, `home_phone`, `cell_phone`, `gender`, `permission`) VALUES
(2, ' dancun', ' ogindo', 'dan@gmail.com', 'dan', '2016-02-22 05:21:01', '0000-00-00 00:00:00', 54657890, 9897654, 'Male ', NULL),
(3, ' james', ' karimu', 'karimu@gmail.com', 'user', '2016-03-05 05:02:07', '1994-02-21 00:00:00', 20756438, 724563421, 'Male ', 2),
(5, '\0', '\0', 'admin@gmail.com', 'admin', '2016-03-19 21:00:00', '1993-06-20 00:00:00', 708426668, 787689090, 'male', 2),
(6, ' joy', ' osumba', 'joy@gmail.com', 'joy', '2016-03-31 03:51:50', '1993-02-12 00:00:00', 2147483647, 2147483647, 'Male ', 1),
(7, ' joy', ' dan', 'joy@gmail.com', 'joy', '2016-03-31 04:08:44', '1993-02-12 00:00:00', 2147483647, 2147483647, 'Male ', 1),
(8, ' joy', ' dan', 'joy@gmail.com', 'joy', '2016-03-31 04:09:16', '1993-02-12 00:00:00', 2147483647, 2147483647, 'Male ', 1),
(9, ' super', ' admin', 'superadmin@gmail.com', 'super', '2016-04-04 04:54:14', '0000-00-00 00:00:00', 362528900, 735362728, 'Male ', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
